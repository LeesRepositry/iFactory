# 企业软件快速开发平台

#### 介绍

      这是一个开源免费的企业管理软件平台，
      它是我平时在工作中积累的一些通用代码、常见功能模块的汇集。
      如果你也是做企业软件开发的，可能会用到它，你可以直接拷贝并使用它。
      这样就不用”重复造轮子“了。
      希望大家共同维护，逐渐积累很多常用功能，减少程序员的开发工作量。
     

#### 软件架构
    使用了Visule Studio 2013进行开发的。
    主要包括：一个登录界面，一个系统主界面。
    然后是关于权限管理的模块
    关于Excel操作的模块
    关于数据库操作的通用助手类。
    以及一些其它的很多企业软件开发中常用的功能。
#### 安装教程

     使用VS 2013直接打开，编译即可

#### 使用说明

    都是常见的，通用性的功能，使用说明，直接看源码注释即可。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
