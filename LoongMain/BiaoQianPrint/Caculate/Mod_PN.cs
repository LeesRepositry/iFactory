﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BiaoQianPrint.Caculate
{
    //更高层次的，List<Mod_PN> listResult 在业务逻辑里面定义。那个需要保存txt么？
    class Mod_PN
    {
        public string mShouHuoDate;
        public string mPN;

        public List<Mod_DaDai> listDaDai = new List<Mod_DaDai>();

        public bool isHaveZhongDai = false;
        public List<Mod_ZhongDai> listZhongDai = new List<Mod_ZhongDai>();
        public bool isHaveXiaoDai = false;
        public List<Mod_XiaoDai> listXiaoDai = new List<Mod_XiaoDai>();

        //自己带一个把  同DC的袋子合并的算法
        public List<Mod_DaDai_BQ> listDaDai_BQ = new List<Mod_DaDai_BQ>();
        public List<Mod_ZhongDai_BQ> listZhongDai_BQ = new List<Mod_ZhongDai_BQ>();
        public List<Mod_XiaoDai_BQ> listXiaoDai_BQ = new List<Mod_XiaoDai_BQ>();

        //外面调用时，遍历 pn的List，然后逐个合并即可
        public void HeBingTongDCDaiZi_OnePN()
        {
            //首先给这几个 都按照DC排序一下会更好。  其实前面每个袋子，都是先 YL里面的DC ,然后是混DC，然后是新DC，因此不需要排序
            Mod_DaDai_BQ ddBq = new Mod_DaDai_BQ();
            foreach (Mod_DaDai dadai in listDaDai)
            {
                if (ddBq.mDC == dadai.mDC)
                {
                    ddBq.ZhangShu++;
                }
                else
                {
                    ddBq = new Mod_DaDai_BQ();//指向一个新对象了。
                    ddBq.mDC = dadai.mDC;
                    ddBq.BOM_WaiNum = dadai.BOM_WaiNum;
                    ddBq.ZhangShu = 1;
                    listDaDai_BQ.Add(ddBq);
                }
            }

            if (isHaveZhongDai == true)
            {
                Mod_ZhongDai_BQ zdBq = new Mod_ZhongDai_BQ();
                foreach (Mod_ZhongDai zhongdai in listZhongDai)
                {
                    if (zdBq.mDC == zhongdai.mDC)
                    {
                        zdBq.ZhangShu++;
                    }
                    else
                    {
                        zdBq = new Mod_ZhongDai_BQ();//指向一个新对象了。
                        zdBq.mDC = zhongdai.mDC;
                        zdBq.BOM_ZhongNum = zhongdai.BOM_ZhongNum;
                        zdBq.ZhangShu = 1;
                        listZhongDai_BQ.Add(zdBq);
                    }
                }
            }

            if (isHaveXiaoDai == true)
            {
                Mod_XiaoDai_BQ xdBq = new Mod_XiaoDai_BQ();
                foreach (Mod_XiaoDai xiaodai in listXiaoDai)
                {
                    if (xdBq.mDC == xiaodai.mDC)
                    {
                        xdBq.ZhangShu++;
                    }
                    else
                    {
                        xdBq = new Mod_XiaoDai_BQ();//指向一个新对象了。
                        xdBq.mDC = xiaodai.mDC;
                        xdBq.BOM_XiaoNum = xiaodai.BOM_XiaoNum;
                        xdBq.ZhangShu = 1;
                        listXiaoDai_BQ.Add(xdBq);
                    }
                }//foreach
            }//if 
        }//HeBingTongDCDaiZi_OnePN()
    }
}
