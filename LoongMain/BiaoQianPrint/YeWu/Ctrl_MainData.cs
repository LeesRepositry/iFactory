﻿using System;
using System.Collections.Generic;
using System.Text;

using BiaoQianPrint.Tools;
namespace BiaoQianPrint.YeWu
{
    //记录当前，最新的BOM，最新的YL和最新的收货。
    //若灭有收货数据，会提示先导入收货后才能  生成标签。
    class Mdl_MainDataItem
    {
        public FilePathType mType { get; set; }
        public string mValue { get; set; }
    }
    class Ctrl_MainData
    {
        private List<Mdl_MainDataItem> _listData = new List<Mdl_MainDataItem>();


        #region 单例模式
        private static Ctrl_MainData instance;
        public static Ctrl_MainData getUserInfoInstance()
        {
            if (instance == null)
            {
                instance = new Ctrl_MainData();
              }
            return instance;
        }
        #endregion

        #region 读写数据接口。
        private void ReadData()
        {
            _listData = FileDbManager.ReadData<Mdl_MainDataItem>(CommonClass.MainDatapath);
            //_listData = FileDbManager.ReadDataFromBFile<Mdl_MainDataItem>(CommonClass.MainDatapath);
        }
        private void SaveData()
        {
            FileDbManager.SaveData<Mdl_MainDataItem>(_listData, CommonClass.MainDatapath);
            //FileDbManager.SaveDataToBFile<Mdl_MainDataItem>(_listData, CommonClass.MainDatapath);
        }
        #endregion

        private Ctrl_MainData()
        {
            ReadData();
        }

        public string getFilePath(FilePathType pType)
        {
            foreach (Mdl_MainDataItem item in _listData)
            {
                if (item.mType == pType)
                {
                    return item.mValue;
                }
            }
            return "";
        }

        public bool UpdateFilePath(FilePathType pType, string pPath)
        {
            foreach(Mdl_MainDataItem item in _listData)
            {
                if (item.mType == pType) 
                {
                    item.mValue = pPath;
                    SaveData();
                    return true;
                }
            }
            return false;
        }
        
    }
}
