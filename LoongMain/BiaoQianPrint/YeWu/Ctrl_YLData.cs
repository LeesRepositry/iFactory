﻿using System;
using System.Collections.Generic;
using System.Text;

using BiaoQianPrint.Tools;
namespace BiaoQianPrint.YeWu
{
    class Mdl_YLRow
    {
        public string PN { get; set; }
        public string DC { get; set; }
        public int YLNum { get; set; }
    }
    //一个 对象  代表一个  文件。
    class Ctrl_YLData
    {
        private string _FileName="";
        public List<Mdl_YLRow> _listData = new List<Mdl_YLRow>();


        #region 读写数据接口。
        private void ReadData()
        {
            //_listAuth = FileDbManager.ReadData<Mdl_AuthItem>(_FilePath_txt);
            _listData = FileDbManager.ReadData<Mdl_YLRow>(CommonClass.YLDBpath+"\\"+_FileName);
        }
        private void SaveData()
        {
            FileDbManager.SaveData<Mdl_YLRow>(_listData, CommonClass.YLDBpath + "\\" + _FileName);
            //FileDbManager.SaveDataToBFile<Mdl_AuthItem>(_listAuth, _FilePath_bin);
        }
        #endregion

        //即是有文件时直接读取数据的，也是无文件时建立一个新文件的，此时返回一个空的listData列表
        public Ctrl_YLData(string pFileName)
        {
            _FileName = pFileName;
            ReadData();
        }
        public void addData(string pPN,string pDC,int pNum,bool isUpdate)
        {
            foreach (Mdl_YLRow item in _listData)
            {
                if (item.PN == pPN && item.DC == pDC)
                {
                    item.YLNum += pNum;
                    if(isUpdate) SaveData();
                    return;
                }
            }
            Mdl_YLRow newItem = new Mdl_YLRow();
            newItem.PN = pPN;
            newItem.DC = pDC;
            newItem.YLNum = pNum;
            _listData.Add(newItem);
            if (isUpdate) SaveData();
        }
        public void SaveDataToFile() { SaveData(); }
    }
}
