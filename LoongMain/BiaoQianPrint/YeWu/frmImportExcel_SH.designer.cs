﻿namespace BiaoQianPrint.YeWu
{
    partial class frmImportExcel_SH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRowCount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnToDB = new System.Windows.Forms.Button();
            this.grd = new System.Windows.Forms.DataGridView();
            this.btnDaoRu = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.filePathBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.openFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.cblList = new System.Windows.Forms.ComboBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnGuoLvByDate = new System.Windows.Forms.Button();
            this.dtpGuoLv = new System.Windows.Forms.DateTimePicker();
            this.lblReceiveNumHJ = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRowCount
            // 
            this.lblRowCount.AutoSize = true;
            this.lblRowCount.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblRowCount.Location = new System.Drawing.Point(887, 15);
            this.lblRowCount.Name = "lblRowCount";
            this.lblRowCount.Size = new System.Drawing.Size(11, 12);
            this.lblRowCount.TabIndex = 24;
            this.lblRowCount.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(853, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 23;
            this.label1.Text = "行数";
            // 
            // btnToDB
            // 
            this.btnToDB.Location = new System.Drawing.Point(218, 44);
            this.btnToDB.Name = "btnToDB";
            this.btnToDB.Size = new System.Drawing.Size(122, 34);
            this.btnToDB.TabIndex = 22;
            this.btnToDB.Text = "保存表格中全部数据";
            this.btnToDB.UseVisualStyleBackColor = true;
            this.btnToDB.Click += new System.EventHandler(this.btnToDB_Click);
            // 
            // grd
            // 
            this.grd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd.Location = new System.Drawing.Point(0, 161);
            this.grd.Name = "grd";
            this.grd.RowTemplate.Height = 23;
            this.grd.Size = new System.Drawing.Size(1106, 344);
            this.grd.TabIndex = 21;
            // 
            // btnDaoRu
            // 
            this.btnDaoRu.Location = new System.Drawing.Point(695, 4);
            this.btnDaoRu.Name = "btnDaoRu";
            this.btnDaoRu.Size = new System.Drawing.Size(75, 34);
            this.btnDaoRu.TabIndex = 20;
            this.btnDaoRu.Text = "读取数据";
            this.btnDaoRu.UseVisualStyleBackColor = true;
            this.btnDaoRu.Click += new System.EventHandler(this.btnDaoRu_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(9, 4);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 34);
            this.btnOpen.TabIndex = 19;
            this.btnOpen.Text = "选择文件";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // filePathBox
            // 
            this.filePathBox.Location = new System.Drawing.Point(90, 10);
            this.filePathBox.Name = "filePathBox";
            this.filePathBox.Size = new System.Drawing.Size(417, 21);
            this.filePathBox.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(776, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 27;
            this.label2.Text = "请核对共计：";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox1.ForeColor = System.Drawing.Color.Purple;
            this.textBox1.Location = new System.Drawing.Point(7, 82);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(955, 73);
            this.textBox1.TabIndex = 28;
            this.textBox1.Text = "【1】导入的Exc第一列必须设置为【日期格式：类型\"2001-3-14\"】，后面三列常规或文本都行；\r\n【2】第一行不会导入，需要空着或写标题；\r\n【3】核对行数" +
    "和收货数量总计；确定无误；\r\n【4】更新选中行时，一次最多选择20行";
            // 
            // openFileDlg
            // 
            this.openFileDlg.FileName = "openFileDialog1";
            // 
            // cblList
            // 
            this.cblList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cblList.FormattingEnabled = true;
            this.cblList.Location = new System.Drawing.Point(513, 10);
            this.cblList.Name = "cblList";
            this.cblList.Size = new System.Drawing.Size(176, 20);
            this.cblList.TabIndex = 29;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(696, 42);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(109, 37);
            this.btnExport.TabIndex = 30;
            this.btnExport.Text = "导出到Excel";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnGuoLvByDate
            // 
            this.btnGuoLvByDate.Location = new System.Drawing.Point(8, 44);
            this.btnGuoLvByDate.Name = "btnGuoLvByDate";
            this.btnGuoLvByDate.Size = new System.Drawing.Size(75, 34);
            this.btnGuoLvByDate.TabIndex = 31;
            this.btnGuoLvByDate.Text = "按日期过滤";
            this.btnGuoLvByDate.UseVisualStyleBackColor = true;
            this.btnGuoLvByDate.Click += new System.EventHandler(this.btnGuoLvByDate_Click);
            // 
            // dtpGuoLv
            // 
            this.dtpGuoLv.Location = new System.Drawing.Point(89, 51);
            this.dtpGuoLv.Name = "dtpGuoLv";
            this.dtpGuoLv.Size = new System.Drawing.Size(119, 21);
            this.dtpGuoLv.TabIndex = 32;
            // 
            // lblReceiveNumHJ
            // 
            this.lblReceiveNumHJ.AutoSize = true;
            this.lblReceiveNumHJ.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblReceiveNumHJ.Location = new System.Drawing.Point(1016, 15);
            this.lblReceiveNumHJ.Name = "lblReceiveNumHJ";
            this.lblReceiveNumHJ.Size = new System.Drawing.Size(11, 12);
            this.lblReceiveNumHJ.TabIndex = 43;
            this.lblReceiveNumHJ.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(935, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 42;
            this.label4.Text = "收货数量总计";
            // 
            // FrmImportExcel_SH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1106, 505);
            this.Controls.Add(this.lblReceiveNumHJ);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtpGuoLv);
            this.Controls.Add(this.btnGuoLvByDate);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.cblList);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblRowCount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnToDB);
            this.Controls.Add(this.grd);
            this.Controls.Add(this.btnDaoRu);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.filePathBox);
            this.Name = "FrmImportExcel_SH";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "收货数据导入";
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRowCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnToDB;
        private System.Windows.Forms.DataGridView grd;
        private System.Windows.Forms.Button btnDaoRu;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.TextBox filePathBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.OpenFileDialog openFileDlg;
        private System.Windows.Forms.ComboBox cblList;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnGuoLvByDate;
        private System.Windows.Forms.DateTimePicker dtpGuoLv;
        private System.Windows.Forms.Label lblReceiveNumHJ;
        private System.Windows.Forms.Label label4;
    }
}