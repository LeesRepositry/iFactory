﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using BiaoQianPrint.Caculate;
using BiaoQianPrint.Tools;
namespace BiaoQianPrint.YeWu
{
    class Ctrl_BiaoQianPrint
    {
        private string _FileName = "";
        private List<Mod_PN> listData = new List<Mod_PN>();

        public Ctrl_BiaoQianPrint(string pFileName,bool isReadData)
        {
            this._FileName = pFileName;
            if(isReadData) ReadDataFromFile();
        }

        public void AddRow(Mod_PN mdlpn)
        {
            listData.Add(mdlpn);
        }

        //【首先把 相同DC的袋子都合并。然后再输出】
        #region
        private void HeBingTongDC_Diazi()
        {

        }
        #endregion

        //文件格式。  一行就是一个 Mod_PN
        // 里面数据项使用#分割。 依次是： SHDate # PN # 大袋list # isHavaZhongDai # 中袋List # isHaveXiaoDai # 小袋list
        // isHaveZhongDai  和 isHaveXiaoDai 用1 和0 表示。
        //各个袋list里面使用 | 分割，一次是 DC - BomNum
        public void SaveDataToFile()
        {
            //如果文件存在，就删除文件
            if (File.Exists(CommonClass.BQDBpath+"\\"+ _FileName))
            {
                File.Delete(CommonClass.BQDBpath + "\\" + _FileName);
            }

            //如果不存在，他会自己创建的。
            FileStream fw = new FileStream(CommonClass.BQDBpath + "\\" + _FileName, FileMode.Create, FileAccess.Write);//创建写入文件 
            StreamWriter sw = new StreamWriter(fw, Encoding.UTF8);

            foreach (Mod_PN row in listData)
            {
                //首先合并
                row.HeBingTongDCDaiZi_OnePN();

                StringBuilder sb = new StringBuilder();
                sb.Append(row.mShouHuoDate + "#");//项目编号string
                sb.Append(row.mPN + "#");//项目名称string
                foreach (Mod_DaDai_BQ ddBq in row.listDaDai_BQ)
                {
                    sb.Append(ddBq.mDC + "-");
                    sb.Append(ddBq.BOM_WaiNum.ToString()+"-");
                    sb.Append(ddBq.ZhangShu.ToString());
                    sb.Append("|");
                }
                sb.Remove(sb.Length - 1, 1);
                //写入中袋
                if (row.isHaveZhongDai == true)
                {
                    sb.Append("#1#");
                    foreach (Mod_ZhongDai_BQ zdBq in row.listZhongDai_BQ)
                    {
                        sb.Append(zdBq.mDC + "-");
                        sb.Append(zdBq.BOM_ZhongNum.ToString()+"-");
                        sb.Append(zdBq.ZhangShu.ToString());
                        sb.Append("|");
                    }
                    sb.Remove(sb.Length - 1, 1);
                }
                else
                {
                    sb.Append("#0#");
                }

                //写入小袋
                if (row.isHaveXiaoDai == true)
                {
                    sb.Append("#1#");
                    foreach (Mod_XiaoDai_BQ xdBq in row.listXiaoDai_BQ)
                    {
                        sb.Append(xdBq.mDC + "-");
                        sb.Append(xdBq.BOM_XiaoNum.ToString()+"-");
                        sb.Append(xdBq.ZhangShu.ToString());
                        sb.Append("|");
                    }
                    sb.Remove(sb.Length - 1, 1);
                }
                else
                {
                    sb.Append("#0#");
                }
                sw.WriteLine(sb);//  去掉最后一个#
            }
            sw.Close();
            fw.Close();
        }

        public void ReadDataFromFile()
        {
            if (!File.Exists(CommonClass.BQDBpath + "\\" + _FileName)) return;
        }
    }
}
