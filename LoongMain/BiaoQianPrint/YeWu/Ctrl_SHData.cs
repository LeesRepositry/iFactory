﻿using System;
using System.Collections.Generic;
using System.Text;

using BiaoQianPrint.Tools;
using System.IO;
namespace BiaoQianPrint.YeWu
{
    class Mdl_SHRow
    {
        public string SHDate { get; set; }
        public string PN { get; set; }
        public string DC { get; set; }
        public int Num { get; set; }
    }
    class Ctrl_SHData
    {
        private string _FileName;
        public List<Mdl_SHRow> listData = new List<Mdl_SHRow>();
        public double ShSum;
        public string SHDate;

        //同DC的要合并。记录合并前行数。为了给导入者对数用。
        private int TongDChebingQianHangShu = 0;

        public Ctrl_SHData(string pFileName)
        {
            _FileName = pFileName;
            //加载数据
            StreamReader sr = new StreamReader(CommonClass.SHDBpath+"\\"+_FileName, Encoding.UTF8);
            String line;
            double dHeJi = 0;
            while ((line = sr.ReadLine()) != null)
            {
                string[] splits = line.Split('#');
                Mdl_SHRow item = new Mdl_SHRow();
                item.SHDate = splits[0];//收货日期
                item.PN = splits[1];//PN
                item.DC =splits[2];//DC
                item.Num =CommonClass.GetIntFromString(splits[3]);//数量
                dHeJi += CommonClass.GetIntFromString(splits[3]);

                listData.Add(item);
            }
            this.ShSum = dHeJi;
            this.SHDate = listData[0].SHDate;
        }

        public void AddItem(string pSHDate,string pPN,string pDC,int pNum)
        {
            //首先判断是否存在 SHDate、PN,DC相同的 有的话先合并。
            //此功能暂时保留。以后增加。

            Mdl_SHRow item = new Mdl_SHRow();
            item.SHDate = pSHDate;
            item.PN = pPN;
            item.DC = pDC;
            item.Num = pNum;

            SHDate = pSHDate;
            ShSum += pNum;
            
            listData.Add(item);
        }
        private void ClearData()
        {
            listData.Clear();
            ShSum = 0;
        }
        public void SaveDataToFile(string pFileName)
        {
            _FileName = pFileName;
            //1、写到txt文件里面
            FileDbManager.SaveData<Mdl_SHRow>(listData, CommonClass.SHDBpath+"\\"+_FileName);

            Ctrl_MainData.getUserInfoInstance().UpdateFilePath(FilePathType.SHFilePath, _FileName);
        }
    }
}
