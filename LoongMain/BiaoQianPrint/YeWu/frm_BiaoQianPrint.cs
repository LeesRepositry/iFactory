﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevComponents.DotNetBar;
namespace BiaoQianPrint.YeWu
{
    public partial class frm_BiaoQianPrint : Office2007Form
    {
        public frm_BiaoQianPrint()
        {
            InitializeComponent();
        }

        #region  控件移动
        private void ResizeControl()
        {
            dgvMain.Width = splitContainer1.Width - 10;
            dgvDetl.Width = splitContainer1.Width - 10;

            dgvMain.Height = splitContainer1.Panel1.Height - 50;
            dgvDetl.Height = splitContainer1.Panel2.Height - 50;
        }
        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            ResizeControl();
        }
        private void frm_BiaoQianPrint_Resize(object sender, EventArgs e)
        {
            ResizeControl();
        }
        #endregion

    }
}
