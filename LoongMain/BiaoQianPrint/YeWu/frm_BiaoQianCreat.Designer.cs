﻿namespace BiaoQianPrint.YeWu
{
    partial class frm_BiaoQianCreat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.dgvDetl = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvMain = new System.Windows.Forms.DataGridView();
            this.txtPN = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDetl_Refresh = new System.Windows.Forms.Button();
            this.linkCreateBiaoQian = new System.Windows.Forms.LinkLabel();
            this.lblHangShu = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDetlHeJi = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDC = new System.Windows.Forms.TextBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dtStart = new System.Windows.Forms.DateTimePicker();
            this.dtEnd = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(490, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 14);
            this.label5.TabIndex = 16;
            this.label5.Text = "行数：";
            // 
            // dgvDetl
            // 
            this.dgvDetl.AllowUserToAddRows = false;
            this.dgvDetl.AllowUserToDeleteRows = false;
            this.dgvDetl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetl.Location = new System.Drawing.Point(4, 39);
            this.dgvDetl.Name = "dgvDetl";
            this.dgvDetl.ReadOnly = true;
            this.dgvDetl.RowTemplate.Height = 23;
            this.dgvDetl.Size = new System.Drawing.Size(807, 247);
            this.dgvDetl.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 10;
            this.label3.Text = "PN号：";
            // 
            // dgvMain
            // 
            this.dgvMain.AllowUserToAddRows = false;
            this.dgvMain.AllowUserToDeleteRows = false;
            this.dgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMain.Location = new System.Drawing.Point(4, 41);
            this.dgvMain.Name = "dgvMain";
            this.dgvMain.ReadOnly = true;
            this.dgvMain.RowTemplate.Height = 23;
            this.dgvMain.Size = new System.Drawing.Size(817, 181);
            this.dgvMain.TabIndex = 9;
            // 
            // txtPN
            // 
            this.txtPN.Location = new System.Drawing.Point(81, 10);
            this.txtPN.Name = "txtPN";
            this.txtPN.Size = new System.Drawing.Size(113, 21);
            this.txtPN.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(215, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 12;
            this.label4.Text = "DC：";
            // 
            // btnDetl_Refresh
            // 
            this.btnDetl_Refresh.Location = new System.Drawing.Point(419, 5);
            this.btnDetl_Refresh.Name = "btnDetl_Refresh";
            this.btnDetl_Refresh.Size = new System.Drawing.Size(65, 28);
            this.btnDetl_Refresh.TabIndex = 14;
            this.btnDetl_Refresh.Text = "刷 新";
            this.btnDetl_Refresh.UseVisualStyleBackColor = true;
            // 
            // linkCreateBiaoQian
            // 
            this.linkCreateBiaoQian.AutoSize = true;
            this.linkCreateBiaoQian.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.linkCreateBiaoQian.Location = new System.Drawing.Point(524, 10);
            this.linkCreateBiaoQian.Name = "linkCreateBiaoQian";
            this.linkCreateBiaoQian.Size = new System.Drawing.Size(85, 19);
            this.linkCreateBiaoQian.TabIndex = 16;
            this.linkCreateBiaoQian.TabStop = true;
            this.linkCreateBiaoQian.Text = "生成标签";
            this.linkCreateBiaoQian.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkCreateBiaoQian_LinkClicked);
            // 
            // lblHangShu
            // 
            this.lblHangShu.AutoSize = true;
            this.lblHangShu.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblHangShu.ForeColor = System.Drawing.Color.Maroon;
            this.lblHangShu.Location = new System.Drawing.Point(550, 14);
            this.lblHangShu.Name = "lblHangShu";
            this.lblHangShu.Size = new System.Drawing.Size(15, 14);
            this.lblHangShu.TabIndex = 19;
            this.lblHangShu.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(606, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 14);
            this.label6.TabIndex = 18;
            this.label6.Text = "数量合计：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "开始日期：";
            // 
            // lblDetlHeJi
            // 
            this.lblDetlHeJi.AutoSize = true;
            this.lblDetlHeJi.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblDetlHeJi.ForeColor = System.Drawing.Color.Maroon;
            this.lblDetlHeJi.Location = new System.Drawing.Point(695, 14);
            this.lblDetlHeJi.Name = "lblDetlHeJi";
            this.lblDetlHeJi.Size = new System.Drawing.Size(15, 14);
            this.lblDetlHeJi.TabIndex = 17;
            this.lblDetlHeJi.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(214, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "结束日期：";
            // 
            // txtDC
            // 
            this.txtDC.Location = new System.Drawing.Point(281, 10);
            this.txtDC.Name = "txtDC";
            this.txtDC.Size = new System.Drawing.Size(113, 21);
            this.txtDC.TabIndex = 13;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(418, 7);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(65, 28);
            this.btnRefresh.TabIndex = 8;
            this.btnRefresh.Text = "刷 新";
            this.btnRefresh.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.Gray;
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.splitContainer1.Panel1.Controls.Add(this.dgvMain);
            this.splitContainer1.Panel1.Controls.Add(this.linkCreateBiaoQian);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.btnRefresh);
            this.splitContainer1.Panel1.Controls.Add(this.dtStart);
            this.splitContainer1.Panel1.Controls.Add(this.dtEnd);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.splitContainer1.Panel2.Controls.Add(this.lblHangShu);
            this.splitContainer1.Panel2.Controls.Add(this.label6);
            this.splitContainer1.Panel2.Controls.Add(this.lblDetlHeJi);
            this.splitContainer1.Panel2.Controls.Add(this.label5);
            this.splitContainer1.Panel2.Controls.Add(this.dgvDetl);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.txtPN);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.btnDetl_Refresh);
            this.splitContainer1.Panel2.Controls.Add(this.txtDC);
            this.splitContainer1.Size = new System.Drawing.Size(855, 517);
            this.splitContainer1.SplitterDistance = 180;
            this.splitContainer1.SplitterWidth = 12;
            this.splitContainer1.TabIndex = 20;
            // 
            // dtStart
            // 
            this.dtStart.Location = new System.Drawing.Point(80, 11);
            this.dtStart.Name = "dtStart";
            this.dtStart.Size = new System.Drawing.Size(113, 21);
            this.dtStart.TabIndex = 1;
            // 
            // dtEnd
            // 
            this.dtEnd.Location = new System.Drawing.Point(280, 11);
            this.dtEnd.Name = "dtEnd";
            this.dtEnd.Size = new System.Drawing.Size(113, 21);
            this.dtEnd.TabIndex = 3;
            // 
            // frm_BiaoQianManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 517);
            this.Controls.Add(this.splitContainer1);
            this.DoubleBuffered = true;
            this.Name = "frm_BiaoQianManage";
            this.Text = "FrmPrintManage";
            this.Resize += new System.EventHandler(this.frm_BiaoQianManage_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgvDetl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvMain;
        private System.Windows.Forms.TextBox txtPN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnDetl_Refresh;
        private System.Windows.Forms.LinkLabel linkCreateBiaoQian;
        private System.Windows.Forms.Label lblHangShu;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDetlHeJi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDC;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DateTimePicker dtStart;
        private System.Windows.Forms.DateTimePicker dtEnd;
    }
}