﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevComponents.DotNetBar;
namespace BiaoQianPrint.YeWu
{
    public partial class frm_YLManage : Office2007Form
    {
        public frm_YLManage()
        {
            InitializeComponent();
        }

        #region 控件位置调整 
        private void frm_YLManage_Resize(object sender, EventArgs e)
        {
            ResizeControl();
        }

        private void ResizeControl()
        {
            dgvMain.Width = splitContainer1.Width - 10;
            dgvDetl.Width = splitContainer1.Width - 10;

            dgvMain.Height = splitContainer1.Panel1.Height - 50;
            dgvDetl.Height = splitContainer1.Panel2.Height - 50;
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            ResizeControl();
        }
        #endregion

    }
}
