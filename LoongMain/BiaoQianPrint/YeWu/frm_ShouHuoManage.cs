﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;
using DevComponents.DotNetBar;
using BiaoQianPrint.Tools;
namespace BiaoQianPrint.YeWu
{
    /*
     * 首先是，遍历文件夹，把所有的文件列表给列出来。  文件名里面有日期。
     * 点击某个文件，  读取文件内容，显示其明细数据，
     */
    public partial class frm_ShouHuoManage : Office2007Form
    {
        public frm_ShouHuoManage()
        {
            InitializeComponent();

            initGridView();
            RefreshMain();
        }

        private void linkImpoortExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmImportExcel_SH frm = new frmImportExcel_SH();
            frm.ShowDialog();
        }

        #region 设置表格 列头等其他属性。
        private void initGridView() 
        {
            string strList = "收货日期|数量|导入时间|数据文件名称";//增加部门
            DGVHelper.initDGVbyString(strList, ref dgvMain);
            dgvMain.Columns[0].Width = 200;
            dgvMain.Columns[1].Width = 150;
            dgvMain.Columns[2].Width = 200;
            dgvMain.Columns[3].Width = 500;
            dgvMain.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvMain.ReadOnly = true;
            dgvMain.AllowUserToAddRows = false;

            strList = "收货日期|PN|DC|数量";//增加部门
            DGVHelper.initDGVbyString(strList, ref dgvDetl);
            dgvDetl.Columns[0].Width = 200;
            dgvDetl.Columns[1].Width = 200;
            dgvDetl.Columns[2].Width = 200;
            dgvDetl.Columns[3].Width = 200;
            dgvDetl.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvDetl.ReadOnly = true;
            dgvDetl.AllowUserToAddRows = false;
        }
        #endregion

        private void RefreshMain()
        {
            DirectoryInfo TheFolder = new DirectoryInfo(System.Windows.Forms.Application.StartupPath+@"\SHDB");
            
            //遍历文件
            foreach (FileInfo NextFile in TheFolder.GetFiles())
            {
                string[] splits = NextFile.Name.Split('_');
                dgvMain.Rows.Add();
                dgvMain.Rows[dgvMain.Rows.Count - 1].Cells[0].Value = splits[3];//收货日期
                dgvMain.Rows[dgvMain.Rows.Count - 1].Cells[1].Value = splits[4].Replace(".txt","");//数量
                dgvMain.Rows[dgvMain.Rows.Count - 1].Cells[2].Value = splits[1]+" "+splits[2].Replace("-",":");//时间
                dgvMain.Rows[dgvMain.Rows.Count - 1].Cells[3].Value = NextFile.Name;//
            }
        }
        private void RefreshDetl()
        {
            dgvDetl.Rows.Clear();
            long dHeJi = 0;

            string filePath = CommonClass.SHDBpath+"\\"+dgvMain.SelectedRows[0].Cells[3].Value.ToString();

            StreamReader sr = new StreamReader(filePath, Encoding.UTF8);
            String line;
            while ((line = sr.ReadLine()) != null)
            {
                string[] splits = line.Split('#');
                dgvDetl.Rows.Add();
                dgvDetl.Rows[dgvDetl.Rows.Count - 1].Cells[0].Value = splits[0];//收货日期
                dgvDetl.Rows[dgvDetl.Rows.Count - 1].Cells[1].Value = splits[1];//PN
                dgvDetl.Rows[dgvDetl.Rows.Count - 1].Cells[2].Value = splits[2];//DC
                dgvDetl.Rows[dgvDetl.Rows.Count - 1].Cells[3].Value = splits[3];//数量
                dHeJi += CommonClass.GetIntFromString(splits[3]);

            }
            lblDetlHeJi.Text = dHeJi.ToString();
            lblHangShu.Text = dgvDetl.Rows.Count.ToString();
        }

        #region 调整控件位置
        private void frm_ShouHuo_Resize(object sender, EventArgs e)
        {
            ResizeControl();
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            ResizeControl();
        }

        private void ResizeControl()
        {
            dgvMain.Width = splitContainer1.Width - 10;
            dgvDetl.Width = splitContainer1.Width - 10;

            dgvMain.Height = splitContainer1.Panel1.Height-50;
            dgvDetl.Height = splitContainer1.Panel2.Height - 50;
        }
        #endregion

        private void dgvMain_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvMain.SelectedRows.Count <= 0) 
                return; 
            else
                RefreshDetl();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshMain();
        }

        private void btnDetl_Refresh_Click(object sender, EventArgs e)
        {
            RefreshDetl();
        }
    }
}
