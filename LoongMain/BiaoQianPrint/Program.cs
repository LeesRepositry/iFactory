﻿using System;
using System.Collections.Generic;

using System.Windows.Forms;

using BiaoQianPrint.sys;

namespace BiaoQianPrint
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            frmLogin frmlog = new frmLogin(1);//"synboaPDM"
            //弹出登录界面
            if (frmlog.ShowDialog() == DialogResult.OK)
            {
                Application.Run(new MDIForm());
            }
        }
    }
}
