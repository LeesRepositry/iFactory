﻿using System;
using System.Collections.Generic;

using System.Text;

using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using BiaoQianPrint.Tools;
namespace BiaoQianPrint.Model
{
    [Serializable]
    public class Mdl_RoleItem
    {
        public string mRoleID{get; set;} //主键
        public string mRoleName{get; set;}
        public string mRemark{get; set;}
        public int mSortNo{get; set;}

        public Mdl_RoleItem() { }

        public Mdl_RoleItem(string pRoleID,string pRoleName,string pRemark,int pSortNo){
            mRoleID=pRoleID;
            mRoleName=pRoleName;
            mRemark=pRemark;
            mSortNo=pSortNo;
        }
    }
    class Ctrl_RoleInfo
    {
        private string _FilePath=Application.StartupPath+"\\Role.dll";
        private List<Mdl_RoleItem> _listRole = new List<Mdl_RoleItem>();
        
        #region 单例模式

        private static Ctrl_RoleInfo instance;
        public static Ctrl_RoleInfo getRoleInfoInstance()
        {
            if (instance == null)
            {
                instance = new Ctrl_RoleInfo();
            }
            return instance;
        }
        #endregion

        #region 本地经常在2种 数据格式间切换，要挨着找所有的地方，因此集中在这儿。这是个接口。
        private void ReadData()
        {
            _listRole = FileDbManager.ReadDataFromBFile<Mdl_RoleItem>(_FilePath);
        }
        private void SaveData()
        {
            FileDbManager.SaveDataToBFile<Mdl_RoleItem>(_listRole, _FilePath);
        }
        #endregion

        public Ctrl_RoleInfo()
        {
            #region // 错误的方法，因为序列化内容里有List的名字，不仅仅是数据     扩展字段是使用
            //_listRole= CommonClass.ReadDataFormFile<Mdl_RoleItem>(_FilePath);
            //foreach (Mdl_RoleItem item in _listRole)
            //{
            //    Mod_RoleInfoItem_Tmp itemTmp = new Mod_RoleInfoItem_Tmp();
            //    itemTmp.mRoleID = item.mRoleID;
            //    itemTmp.mRoleName = item.mRoleName;
            //    itemTmp.mRemark = "";
            //    itemTmp.mSortNo = item.mSortNo;
            //    _listRole_Tmp.Add(itemTmp);
            //}
            // CommonClass.WriteDataToFile<Mod_RoleInfoItem_Tmp>(_listRole_Tmp,_FilePath);
            #endregion
            ReadData();
            
        }
        public void SortList()
        {
            _listRole.Sort(delegate(Mdl_RoleItem x, Mdl_RoleItem y)
            {
                return x.mSortNo.CompareTo(y.mSortNo);
            });
        }
        #region 增 删 改
        public bool InserOneRow(string pRoleName, string pRemark, int pSortNo)
        {
            //if (getRoleItemByID(pRoleID) != null)
            //{
            //    return;
            //}
            string pRoleID = CommonClass.getNewID<Mdl_RoleItem>(_listRole,"mRoleID");
            Mdl_RoleItem item = new Mdl_RoleItem(pRoleID, pRoleName, pRemark, pSortNo);
            this._listRole.Add(item);
            SaveData();
            return true;
        }
        public bool UpdateRow(Mdl_RoleItem pItem)
        {

            foreach (Mdl_RoleItem item in _listRole)
            {
                if (item.mRoleID == pItem.mRoleID)
                {
                    item.mRoleName = pItem.mRoleName;
                    item.mRemark = pItem.mRemark;
                    item.mSortNo = pItem.mSortNo;
                    SaveData();
                    return true;
                }
            }
            return false;
        }
        public bool DelByRoleID(string pRoleID)
        {
            foreach (Mdl_RoleItem item in _listRole)
            {
                if (item.mRoleID == pRoleID)
                {
                    _listRole.Remove(item);
                    SaveData();
                    return true;
                }
            }
            return false;
        }
        #endregion

        // 返回所有的 Role数据
        public List<Mdl_RoleItem> GetAllRole()
        {
            return this._listRole;
        }

        //根据 ID 返回 RoleName
        public Mdl_RoleItem getRoleItemByID(string pRoleID)
        {
            foreach (Mdl_RoleItem item in this._listRole)
            {
                if (item.mRoleID == pRoleID)
                {
                    return item;
                }
            }
            return null;
        }
    }
}
