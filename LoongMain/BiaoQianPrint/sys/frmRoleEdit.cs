using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BiaoQianPrint.Model;
using BiaoQianPrint.Tools;
namespace BiaoQianPrint.sys
{
    public partial class frmRoleEdit : Form
    {
        public int EditType=1;//1是新增，3是修改。

        private Mdl_RoleItem _RoleItem=new Mdl_RoleItem();
        public frmRoleEdit(int pEditType,Mdl_RoleItem pRoleItem)
        {
            EditType = pEditType;
            InitializeComponent();
            if (EditType == 3)
            {
                _RoleItem =pRoleItem;
                textBox1.Text = _RoleItem.mRoleID;
                textBox2.Text = _RoleItem.mRoleName;
                textBox3.Text = _RoleItem.mRemark;
                textBox4.Text = _RoleItem.mSortNo.ToString();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            _RoleItem.mRoleName = textBox2.Text.ToString();
            _RoleItem.mRemark = textBox3.Text.ToString();
            _RoleItem.mSortNo =CommonClass.GetIntFromString(textBox4.Text.ToString());

            if (_RoleItem.mSortNo == 0) 
            {
                MessageBox.Show("序号必须输入，并大于0！");
                return;
            }
            //验证必须是 汉字，字母和数字
            if (CommonClass.RegexName(_RoleItem.mRoleName) == false)
            {
                MessageBox.Show("角色名称必须是汉字、字母或数字");
                return;
            }

            if (_RoleItem.mRoleName.Length > 23)
            {
                MessageBox.Show("角色名称不能超过24个字符！");
                return;
            }

            bool result = false;
            if (EditType == 1)
            {
                result=Ctrl_RoleInfo.getRoleInfoInstance().InserOneRow(_RoleItem.mRoleName,_RoleItem.mRemark,_RoleItem.mSortNo);
            }
            else
            {
                result=Ctrl_RoleInfo.getRoleInfoInstance().UpdateRow(_RoleItem);
            }
            if (result == true)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}