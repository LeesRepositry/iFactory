using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BiaoQianPrint.Model;
using DevComponents.DotNetBar;
namespace BiaoQianPrint.sys
{
    public partial class frmAuthManager : Office2007Form
    {
        public frmAuthManager()
        {
            InitializeComponent();

            initCmbList();
            QueryInfo();
        }

        #region 初始化 角色下来列表。
        private void initCmbList()
        {
            List<Mdl_RoleItem> listRole = Ctrl_RoleInfo.getRoleInfoInstance().GetAllRole();

            foreach (Mdl_RoleItem item in listRole)
            {
                Tools.mitem pitem = new Tools.mitem(item.mRoleID,item.mRoleName, 0);
                this.comboBox1.Items.Add(pitem);
            }
            if (comboBox1.Items != null && comboBox1.Items.Count > 0)
            {
                comboBox1.SelectedIndex = 0;
            }
        }
        #endregion

        private void QueryInfo()
        {
            //初始化跟节点。
            treeView1.Nodes.Clear();
            TreeNode node = treeView1.Nodes.Add("T", "标签打印系统");
            node.ImageIndex = 1;
            node.Tag = "T";
            
            // 找到每个第一层的子，显示出来。
            recursionShow(node);
            treeView1.ExpandAll();
        }
        private void recursionShow(TreeNode pnode)
        {
            List<Mdl_AuthItem> listAuth = Ctrl_AuthInfo.getUserInfoInstance().GetAllAuthByFid(pnode.Tag.ToString());
            foreach (Mdl_AuthItem item in listAuth)
            {
                TreeNode node = pnode.Nodes.Add(item.mAuthID, item.mAuthName);
                node.Tag = item.mAuthID;
                if (Ctrl_RoleAuth.getRoleInfoInstance().IsHaveAuth(item.mAuthID,((Tools.mitem)comboBox1.SelectedItem).ItemID))
                {
                    node.ImageIndex = 1;
                }
                else
                {
                    node.ImageIndex = 0;
                }
                recursionShow(node);
            }
        }
        private void frmAuthManager_Resize(object sender, EventArgs e)
        {
            this.treeView1.Width = this.Width - 50;
            this.treeView1.Height = this.Height - treeView1.Top - 30;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //提示是否要保存数据？
            QueryInfo();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            treeView1.SelectedNode.SelectedImageIndex = treeView1.SelectedNode.ImageIndex;
        }

        #region TreeView的 按键事件
        private void treeView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (treeView1.SelectedNode.Tag.ToString() == "T") return;
            if (treeView1.SelectedNode.ImageIndex == 0)  //这是设为true
            {
                treeView1.SelectedNode.SelectedImageIndex = 1;
                treeView1.SelectedNode.ImageIndex=1;
                //父节点，顶多3层。全部置为1
                YesTreeViewNode(treeView1.SelectedNode.Parent);
                //同时把自己的子节点都置为1
                foreach (TreeNode n in treeView1.SelectedNode.Nodes)
                {
                    if (n.ImageIndex == 0)   //=0的不用管了。
                    {
                        n.ImageIndex = 1;
                    }
                }

            }
            else
            {
                treeView1.SelectedNode.SelectedImageIndex = 0;
                treeView1.SelectedNode.ImageIndex = 0;
                //同时遍历子节点，全部置为0
                NoTreeViewNode(treeView1.SelectedNode.Nodes);
            }
            
        }
        #endregion

        private void btnSave_Click(object sender, EventArgs e)
        {
            //if (((Tools.mitem)comboBox1.SelectedItem).ItemID == "R001")
            //{
            //    MessageBox.Show("管理员的权限不能更改！");
            //    return;
            //}
            //Tools.KMSCommon.LogInfo("清空角色权限，角色名称：" + ((Tools.mitem)comboBox1.SelectedItem).ItemName, "");
            Ctrl_RoleAuth.getRoleInfoInstance().ClearData(((Tools.mitem)comboBox1.SelectedItem).ItemID);
            //遍历 树，写入值
            PrintTreeViewNode(treeView1.Nodes);
            Ctrl_RoleAuth.getRoleInfoInstance().AfterInsert();
            MessageBox.Show("保存成功！");
        }
        public void PrintTreeViewNode(TreeNodeCollection nodes)
        {
            foreach (TreeNode n in nodes)
            {
                //Response.Write(n.Text + ",");
                //处理网数据库写入数据
                if (n.ImageIndex == 1)   //=0的不用管了。
                {
                    Ctrl_RoleAuth.getRoleInfoInstance().InsertOneRow(((Tools.mitem)comboBox1.SelectedItem).ItemID, n.Tag.ToString());
                    //Tools.KMSCommon.LogInfo("添加角色权限，角色名称：" + ((Tools.mitem)comboBox1.SelectedItem).ItemName + " ；权限名称：" + n.Text.ToString(), "");
                }
                PrintTreeViewNode(n.Nodes);
            }
        }
        //是把子节点都置为0
        public void NoTreeViewNode(TreeNodeCollection nodes)
        {
            foreach (TreeNode n in nodes)
            {
                if (n.ImageIndex == 1)   //=0的不用管了。
                {
                    n.ImageIndex = 0;
                }
                NoTreeViewNode(n.Nodes);
            }
        }
        //把父节点，全部置为1
        public void YesTreeViewNode(TreeNode node)
        {
            if(node.Tag.ToString()!="T")
            {
                node.ImageIndex = 1;
                YesTreeViewNode(node.Parent);
            }
            
        }
    }
}