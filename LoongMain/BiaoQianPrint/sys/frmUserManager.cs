using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BiaoQianPrint.Model;
using DevComponents.DotNetBar;
namespace BiaoQianPrint.sys
{
    public partial class frmUserManager : Office2007Form
    {
        public frmUserManager()
        {
            InitializeComponent();

        //  用户账号,用户名称,角色编号,角色名称,账号状态,创建人,创建时间
            dataGridView1.ReadOnly = true;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;
            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.ColumnHeader;
            QueryInfo();
        }
        private void QueryInfo()
        {
            this.dataGridView1.Rows.Clear();
            foreach (Mdl_UserRow item in Mod_UserInfo.getUserInfoInstance().GetAllUser())
            {
                int index = this.dataGridView1.Rows.Add();
                //     0        1         2        3        4        5          6         7       8       9      10       
                //  用户编号,角色编号，角色名称,用户账号,员工姓名,账号状态,密码修改时间,创建人,创建时间,修改人,修改时间,
                this.dataGridView1.Rows[index].Cells[0].Value = item.UserID;
                this.dataGridView1.Rows[index].Cells[1].Value = item.RoleID;
                this.dataGridView1.Rows[index].Cells[2].Value = Ctrl_RoleInfo.getRoleInfoInstance().getRoleItemByID(item.RoleID).mRoleName;
                this.dataGridView1.Rows[index].Cells[3].Value = item.UserLogID;
                this.dataGridView1.Rows[index].Cells[4].Value = item.UserName;
                this.dataGridView1.Rows[index].Cells[5].Value = item.UserStatus;
                
                this.dataGridView1.Rows[index].Cells[6].Value = item.ChangePasswordTime;
                this.dataGridView1.Rows[index].Cells[7].Value = item.CreateUserName+"";
                this.dataGridView1.Rows[index].Cells[8].Value = item.CreateTime;
                this.dataGridView1.Rows[index].Cells[9].Value = item.EditUserName+"";
                this.dataGridView1.Rows[index].Cells[10].Value = item.EditTime;
            }
            //dataGridView1.Columns[5].Width = 150;
            //dataGridView1.Columns[6].Width = 120;
            //dataGridView1.Columns[7].Width = 120;
        }
        private void frmUserManager_Resize(object sender, EventArgs e)
        {
            this.dataGridView1.Width = this.Width - 20;
            this.dataGridView1.Height = this.Height - dataGridView1.Top - 20;
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            //if (!Tools.KMSCommon.IsHaveAuth("T10101")) return;
            frmUserEdit frm = new frmUserEdit(1, null);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                QueryInfo();
            }
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            QueryInfo();
            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.ColumnHeader;
            
        }

        private void btnDeleRow_Click(object sender, EventArgs e)
        {
            //if (!Tools.KMSCommon.IsHaveAuth("T10103")) return;
            DialogResult TS = MessageBox.Show("您确定要删除么？", "提示：删除后将不能恢复！", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (TS == DialogResult.No) return;


            if (dataGridView1.SelectedRows.Count <= 0) { MessageBox.Show("请选择一行数据！"); return; }

            //if (dataGridView1.SelectedRows[0].Cells[2].Value.ToString() != "编辑中") { MessageBox.Show("只有编辑中的项目可以删除！"); return; }
            if (Mod_UserInfo.getUserInfoInstance().DelByUserID(dataGridView1.SelectedRows[0].Cells[0].Value.ToString()))
            {
                MessageBox.Show("删除成功！");
            }
            else { MessageBox.Show("删除时出错！"); }
            QueryInfo();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (!Tools.KMSCommon.IsHaveAuth("T10102")) return;
            if (dataGridView1.SelectedRows.Count == 0) { MessageBox.Show("未选择行！"); return; }
            if (dataGridView1.SelectedRows[0].Cells[0].Value is DBNull) return;
            if (dataGridView1.SelectedRows[0].Cells[0].Value == null) return;

            //     0        1         2        3        4        5          6         7       8       9      10       
            //  用户编号,角色编号，角色名称,用户账号,员工姓名,账号状态,密码修改时间,创建人,创建时间,修改人,修改时间,
            Mdl_UserRow user = new Mdl_UserRow();
            user.UserID = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            user.RoleID = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            //user.RolsName = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            user.UserLogID = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            user.UserName = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            user.UserStatus = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[5].Value.ToString());
            user.ChangePasswordTime = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();
            user.CreateUserName = dataGridView1.SelectedRows[0].Cells[7].Value.ToString();
            user.CreateTime = dataGridView1.SelectedRows[0].Cells[8].Value.ToString();
            user.EditUserName = dataGridView1.SelectedRows[0].Cells[9].Value.ToString();
            user.EditTime = dataGridView1.SelectedRows[0].Cells[10].Value.ToString();
            frmUserEdit frm = new frmUserEdit(3, user);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                //修改完成，更新数据。
                QueryInfo();
            }
        }

    }
}