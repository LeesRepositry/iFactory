using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BiaoQianPrint.Model;
using DevComponents.DotNetBar;
namespace BiaoQianPrint.sys
{
    public partial class frmUserCheckManager : Office2007Form
    {
        public frmUserCheckManager()
        {
            InitializeComponent();
            //  用户账号,用户名称,角色编号,角色名称,账号状态,创建人,创建时间

            dataGridView1.ReadOnly = true;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;
            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.ColumnHeader;
            QueryInfo();
        }
        private void QueryInfo()
        {
            List<Mdl_UserRow> listUser = Mod_UserInfo.getUserInfoInstance().GetAllUser();
            this.dataGridView1.Rows.Clear();
            foreach (Mdl_UserRow item in listUser)
            {
                int index = this.dataGridView1.Rows.Add();
                this.dataGridView1.Rows[index].Cells[0].Value = item.UserID;
                this.dataGridView1.Rows[index].Cells[1].Value = item.UserName;
                this.dataGridView1.Rows[index].Cells[2].Value = item.RoleID;
                this.dataGridView1.Rows[index].Cells[3].Value = Ctrl_RoleInfo.getRoleInfoInstance().getRoleItemByID(item.RoleID).mRoleName;
                this.dataGridView1.Rows[index].Cells[4].Value = item.UserStatus;
                this.dataGridView1.Rows[index].Cells[5].Value = item.CreateUserName;
                this.dataGridView1.Rows[index].Cells[6].Value = item.CreateTime;

            }
            //dataGridView1.Columns[5].Width = 150;
            //dataGridView1.Columns[6].Width = 120;
            //dataGridView1.Columns[7].Width = 120;
        }
        private void frmUserCheckManager_Resize(object sender, EventArgs e)
        {
            this.dataGridView1.Width = this.Width - 20;
            this.dataGridView1.Height = this.Height - dataGridView1.Top - 20;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //if (Tools.KMSCommon.IsHaveAuth("T104") != true)//
            //{
            //    return;
            //}
            if (dataGridView1.SelectedRows[0].Cells[4].Value.ToString() != "0")
            {
                MessageBox.Show("只用状态0的用户才可以审核通过！");
                return;
            }

            if (Mod_UserInfo.getUserInfoInstance().QiyongUser(dataGridView1.SelectedRows[0].Cells[0].Value.ToString()))
            {
                MessageBox.Show("操作成功！");
                QueryInfo();
            }
            else
            {
                MessageBox.Show("操作失败！");
            }
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            //if (Tools.KMSCommon.IsHaveAuth("T104") != true)//
            //{
            //    return;
            //}
            if (dataGridView1.SelectedRows[0].Cells[4].Value.ToString() != "1")
            {
                MessageBox.Show("只用状态1的用户才可以禁用账号！");
                return;
            }

            if (Mod_UserInfo.getUserInfoInstance().DongjieUser(dataGridView1.SelectedRows[0].Cells[0].Value.ToString()))
            {
                MessageBox.Show("操作成功！");
                QueryInfo();
            }
            else
            {
                MessageBox.Show("操作失败！");
            }
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            QueryInfo();
        }
    }
}