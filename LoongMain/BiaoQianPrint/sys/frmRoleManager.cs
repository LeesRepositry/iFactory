using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BiaoQianPrint.Model;
using DevComponents.DotNetBar;
using BiaoQianPrint.Tools;
namespace BiaoQianPrint.sys
{
    public partial class frmRoleManager : Office2007Form
    {
        public frmRoleManager()
        {
            InitializeComponent();

            const string strList = "角色编号|角色名称|备注|排序";//增加部门
            DGVHelper.initDGVbyString(strList,ref dataGridView1);
            //dataGridView1.Columns[0].Width = 60;
            //dataGridView1.Columns[1].Width = 80;
            //dataGridView1.Columns[2].Width = 150;
            //dataGridView1.Columns[3].Width = 80;
            //dataGridView1.Columns[4].Width = 60;
            //dataGridView1.Columns[5].Width = 60;
            //dataGridView1.Columns[4].DefaultCellStyle.BackColor = Color.YellowGreen;
            //dataGridView1.Columns[4].ReadOnly = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;

            QueryInfo();
        }
        private void QueryInfo()
        {
            Ctrl_RoleInfo.getRoleInfoInstance().SortList();
            List<Mdl_RoleItem> listRole = Ctrl_RoleInfo.getRoleInfoInstance().GetAllRole();
            this.dataGridView1.Rows.Clear();
            foreach (Mdl_RoleItem item in listRole)
            {
                int index = this.dataGridView1.Rows.Add();
                this.dataGridView1.Rows[index].Cells[0].Value = item.mRoleID;
                this.dataGridView1.Rows[index].Cells[1].Value = item.mRoleName;
                this.dataGridView1.Rows[index].Cells[2].Value = item.mRemark;
                this.dataGridView1.Rows[index].Cells[3].Value = item.mSortNo;
            }
        }
        private void frmRoleManager_Resize(object sender, EventArgs e)
        {
            this.dataGridView1.Width = this.Width - 20;
            this.dataGridView1.Height = this.Height - dataGridView1.Top - 20;
        }
        
        private void btnQuery_Click(object sender, EventArgs e)
        {
            QueryInfo();
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            //if (!Tools.KMSCommon.IsHaveAuth("T10201")) return;

            frmRoleEdit frm = new frmRoleEdit(1, null);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                QueryInfo();
            }
        }

        private void btnDeleRow_Click(object sender, EventArgs e)
        {
            //if (!Tools.KMSCommon.IsHaveAuth("T10203")) return;
            DialogResult TS = MessageBox.Show("您确定要删除么？", "提示：删除后将不能恢复！", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (TS == DialogResult.No) return;


            if (dataGridView1.SelectedRows.Count <= 0) { MessageBox.Show("请选择一行数据！"); return; }

            //if (dataGridViewX1.SelectedRows[0].Cells[2].Value.ToString() != "编辑中") { MessageBox.Show("只有编辑中的项目可以删除！"); return; }


            
            if (Ctrl_RoleInfo.getRoleInfoInstance().DelByRoleID(dataGridView1.SelectedRows[0].Cells[0].Value.ToString()))
            {
                MessageBox.Show("删除成功！");
            }
            else { MessageBox.Show("删除时出错！"); }
            QueryInfo();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (!Tools.KMSCommon.IsHaveAuth("T10202")) return;

            if (dataGridView1.SelectedRows.Count == 0) { MessageBox.Show("未选择行！"); return; }
            if (dataGridView1.SelectedRows[0].Cells[0].Value is DBNull) return;
            if (dataGridView1.SelectedRows[0].Cells[0].Value == null) return;


            Mdl_RoleItem roleItem = new Model.Mdl_RoleItem();
            roleItem.mRoleID = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            roleItem.mRoleName = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            roleItem.mRemark = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            roleItem.mSortNo =CommonClass.GetIntFromString(dataGridView1.SelectedRows[0].Cells[3].Value.ToString());

            frmRoleEdit frm = new frmRoleEdit(3, roleItem);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                //修改完成，更新数据。
                QueryInfo();
            }
        }

    }
}