using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BiaoQianPrint.Tools;
using BiaoQianPrint.Model;
using System.Configuration;
namespace BiaoQianPrint.sys
{
    public partial class frmLogin : Form
    {
        private Point mouseOffset;//坐标
        private bool isMouseDown = false;//是否鼠标落下
        public static bool _VLPFlag = true;
        //private int mCuoWuCiShu = 0;
        private int LogIndex;
        public frmLogin(int pLogIndex)
        {
            InitializeComponent();
            LogIndex = pLogIndex;
            string mUserID = System.Configuration.ConfigurationSettings.AppSettings["DefaultUserID"];
            if (mUserID.Length != 0)
            {
                txtLogedName.Text = mUserID;
            }
            else
            {
                txtLogedName.Text = "admin";
            }

            
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }
        #region 鼠标拖动
        private void frmLogin_MouseMove(object sender, MouseEventArgs e)
        {

            if (isMouseDown)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos;
            }
        }

        private void frmLogin_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMouseDown = false;
            }
        }

        private void frmLogin_MouseDown(object sender, MouseEventArgs e)
        {
            int xOffset;
            int yOffset;

            if (e.Button == MouseButtons.Left)
            {
                xOffset = -e.X;// -SystemInformation.FrameBorderSize.Width;
                yOffset = -e.Y;// -SystemInformation.CaptionHeight - SystemInformation.FrameBorderSize.Height;
                mouseOffset = new Point(xOffset, yOffset);
                isMouseDown = true;
            }
        }
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMouseDown = false;
            }   
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos;
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            int xOffset;
            int yOffset;

            if (e.Button == MouseButtons.Left)
            {
                xOffset = -e.X-10;// -SystemInformation.FrameBorderSize.Width;
                yOffset = -e.Y-10;// -SystemInformation.CaptionHeight - SystemInformation.FrameBorderSize.Height;
                mouseOffset = new Point(xOffset, yOffset);
                isMouseDown = true;
            }
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            string mLogUserID = txtLogedName.Text.ToString().Trim();
            string mLogUserPassword = txtPassword.Text.ToString().Trim();

            if (mLogUserID.Length == 0) { MessageBox.Show("未填写用户名！"); return; }
            if (mLogUserPassword.Length == 0) { MessageBox.Show("未填写密码！"); return; }

            //是否被禁用了30分钟
            //if (mLogUserID == "admin" | mLogUserID == "secure_admin" | mLogUserID == "log_admin")
            //{
            //    string mJinYongShiJian = System.Configuration.ConfigurationSettings.AppSettings["JinYongShiJian"];
            //    DateTime dOld = DateTime.Parse(mJinYongShiJian);
            //    DateTime dNew = DateTime.Now;
            //    TimeSpan ts = dNew - dOld;
            //    if (ts.TotalMinutes <= 30)
            //    {
            //        MessageBox.Show("管理员帐号被禁用30分钟，请等时间到了再登录！");
            //        this.DialogResult = DialogResult.No;
            //        return;
            //    }

            //}

            Mod_UserInfo mUserInfo = Mod_UserInfo.getUserInfoInstance();
            Mdl_UserRow mUser = mUserInfo.GetUserByUserID(mLogUserID);
            if (mUser == null)
            {
                MessageBox.Show("没有这个账号！");
            }
            else
            {
                //string s = Tools.MD5Tool.GetMD5(mLogUserPassword);
                if (CommonClass.GetMD5(mLogUserPassword) == mUser.Password)
                {
                    Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.AppSettings.Settings["DefaultUserID"].Value = txtLogedName.Text.ToString().Trim();
                    //一定要记得保存，写不带参数的config.Save()也可以
                    config.Save(ConfigurationSaveMode.Modified);
                    //刷新，否则程序读取的还是之前的值（可能已装入内存）
                    System.Configuration.ConfigurationManager.RefreshSection("appSettings");

                    //如果没有审核通过的账号，不能登录。
                    if ((mUser.UserStatus != 1) & (!(mLogUserID == "admin" )))
                    {
                        MessageBox.Show("此账号还为审核通过，不能登录！");
                        return;
                    }
                    //如果密码是默认密码，要求必须修改
                    if (mLogUserPassword == "000000")
                    {
                        MessageBox.Show("第一次登陆，必须修改密码");
                        frmLogUpdatePassword frm = new frmLogUpdatePassword(mLogUserID);
                        frm.ShowDialog();
                        return;
                    }


                    Tools.CommonClass.LogInfo("用户登录", "");
                    CommonClass.mCurrentUser = mUser;
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("密码错误！");
                    //mCuoWuCiShu++;

                    //if (mCuoWuCiShu == 3)
                    //{
                    //    if (mLogUserID == "admin")
                    //    {
                    //        MessageBox.Show("密码连续错误三次！系统被冻结，请半小时后在登录！");
                    //        Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    //        config.AppSettings.Settings["JinYongShiJian"].Value = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                    //        //一定要记得保存，写不带参数的config.Save()也可以
                    //        config.Save(ConfigurationSaveMode.Modified);
                    //        //刷新，否则程序读取的还是之前的值（可能已装入内存）
                    //        System.Configuration.ConfigurationManager.RefreshSection("appSettings");
                    //    }
                    //    else
                    //    {
                    //        MessageBox.Show("密码连续错误三次！帐号被禁用。");
                    //        Manager.Mnanger_User manager = new Manager.Mnanger_User();
                    //        manager.UserNoByName(mLogUserID);
                    //    }
                    //}
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Application.Exit();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #region 关闭按钮事件
        private void picClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void picClose_MouseEnter(object sender, EventArgs e)
        {
            picClose.Image = BiaoQianPrint.Properties.Resources.close1;
        }

        private void picClose_MouseLeave(object sender, EventArgs e)
        {
            picClose.Image = BiaoQianPrint.Properties.Resources.close0;
        }
        #endregion

    }
}