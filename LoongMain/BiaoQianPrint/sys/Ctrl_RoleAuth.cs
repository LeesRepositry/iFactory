﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;
using BiaoQianPrint.Tools;
namespace BiaoQianPrint.Model
{
     class Mdl_RoleAuthItem
    {
         public string mRoleID { get; set; }
         public string mAuthID { get; set; }

        public Mdl_RoleAuthItem() { }

        public Mdl_RoleAuthItem(string pRoleID, string pAuthID)
        {
            mRoleID = pRoleID;
            mAuthID = pAuthID;
        }
    }
    class Ctrl_RoleAuth
    {
        private string _FilePath_bin = System.Windows.Forms.Application.StartupPath + "\\Ra.dll";
        private string _FilePath_txt = System.Windows.Forms.Application.StartupPath + "\\Ra.txt";
        private List<Mdl_RoleAuthItem> _listRoleAuth = new List<Mdl_RoleAuthItem>();

        #region 单例模式
        private static Ctrl_RoleAuth instance;
        public static Ctrl_RoleAuth getRoleInfoInstance()
        {
            if (instance == null)
            {
                instance = new Ctrl_RoleAuth();
            }
            return instance;
        }
        #endregion

        #region 读写Data接口。
        private void ReadData()
        {
            //_listRoleAuth = FileDbManager.ReadData<Mdl_RoleAuthItem>(_FilePath_txt);
            _listRoleAuth = FileDbManager.ReadDataFromBFile<Mdl_RoleAuthItem>(_FilePath_bin);
        }
        private void SaveData()
        {
            //FileDbManager.SaveData<Mdl_AuthItem>(_listAuth, _FilePath_txt);
            FileDbManager.SaveDataToBFile<Mdl_RoleAuthItem>(_listRoleAuth, _FilePath_bin);
        }
        #endregion

        public Ctrl_RoleAuth()
        {
            ReadData();
        }

        /*C#遍历List并删除某个或者几个元素的方法，你的第一反应使用什么方法实现呢？foreach？ for？
        如果是foreach，那么恭喜你，你答错了。如果你想到的是用for，那么你只是离成功进了一步。
        正确的做法是用for倒序遍历，根据条件删除。下面我们用代码来演示foreach，for删除list数据的情况：*/

        #region 权限分配界面所用。
        public void ClearData(string pRoleID)
        {

            for (int i = _listRoleAuth.Count - 1; i >= 0; i--)
            {
                if (_listRoleAuth[i].mRoleID == pRoleID)
                    _listRoleAuth.Remove(_listRoleAuth[i]);
            }
        }
        public void InsertOneRow(string pRoleID,string pAuthID){
            Mdl_RoleAuthItem item = new Mdl_RoleAuthItem(pRoleID, pAuthID);
            this._listRoleAuth.Add(item);
        }

        public void AfterInsert()
        {
            this.SaveData();
        }
        #endregion

        #region 是否拥有权限
        public bool IsHaveAuth(string pAuthID, string pRoleID)
        {
            //if (Tools.CommonClass.mCurrentUser.UserLogID == "admin") return true;

            bool isExsist = false;
            foreach (Mdl_RoleAuthItem item in _listRoleAuth)
            {
                if (item.mAuthID == pAuthID && item.mRoleID == pRoleID)
                {
                    isExsist = true;
                }
            }
            Tools.CommonClass.LogInfo("权限验证。权限编号：" + pAuthID, "");
            return isExsist;
        }
        public bool IsHaveAuth(string pAuthID)
        {
            return IsHaveAuth(pAuthID, Tools.CommonClass.mCurrentUser.RoleID);
        }
        #endregion
    }
}
