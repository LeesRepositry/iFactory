using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BiaoQianPrint.Tools;
using BiaoQianPrint.Model;

namespace BiaoQianPrint.sys
{
    public partial class frmUserEdit : Form
    {
        public int EditType = 1;// 1表示新增， 3表示修改
        Mdl_UserRow mUser=new Mdl_UserRow();

        public frmUserEdit(int pEditType, Mdl_UserRow pUser)
        {
            InitializeComponent();

            EditType = pEditType;            
            //初始化 角色下来列表。
            initCmbList();
            //     0        1       2        3        4            5                6            7           8       9       10      11
            //  用户编号,角色编号,角色名称,用户账号,账号状态,密码最后修改时间,账号有效期开始,账号有效期结束,创建人,创建时间,修改人,修改时间,
            if (EditType == 3)
            {
                mUser = pUser;
                txtUserID.Text = mUser.UserID;
                txtRoleID.Text = mUser.RoleID;
                comboBox1.Text = Ctrl_RoleInfo.getRoleInfoInstance().getRoleItemByID(mUser.RoleID).mRoleName;
                txtLogName.Text = mUser.UserLogID;
                txtUserName.Text = mUser.UserName;
                txtUserStatus.Text = mUser.UserStatus.ToString();
                txtChangePasswordTime.Text = mUser.ChangePasswordTime;
                txtCreateName.Text = mUser.CreateUserName;
                txtCreateTime.Text = mUser.CreateTime;
                txtEditName.Text = mUser.EditUserName;
                txtEditTime.Text = mUser.EditTime;
            }
        }

        #region 初始化 角色下来列表。
        
        private void initCmbList()
        {
            foreach (Mdl_RoleItem dr in Ctrl_RoleInfo.getRoleInfoInstance().GetAllRole())
            {
                Tools.mitem pitem = new Tools.mitem(dr.mRoleID, dr.mRoleName, 0);
                this.comboBox1.Items.Add(pitem);
            }
            if (comboBox1.Items != null && comboBox1.Items.Count > 0)
            {
                comboBox1.SelectedIndex = 0;
            }
        }

        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            //能改的只有4个选项。
            mUser.RoleID = ((mitem)comboBox1.SelectedItem).ItemID;
            mUser.UserName = txtUserName.Text.ToString().Trim();

            if (mUser.UserName.Length == 0) { MessageBox.Show("必须输入用户名称！"); return; }

            bool result = false;
            
            if (EditType == 1)
            {
                mUser.UserLogID = txtLogName.Text.ToString();
                result=Mod_UserInfo.getUserInfoInstance().AddNewUser(mUser);
            }
            else
            {
                result = Mod_UserInfo.getUserInfoInstance().UpdateUser(mUser);
            }

            if (result == true)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnClearPassword_Click(object sender, EventArgs e)
        {
            Mod_UserInfo.getUserInfoInstance().ClearPassword(mUser.UserID,"000000");
            MessageBox.Show("密码被重置为：000000");
        }
    }
}