﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BiaoQianPrint.YeWu;
using BiaoQianPrint.Tools;
using DevComponents.DotNetBar;
namespace BiaoQianPrint.sys
{
    public partial class frmZCMManage : Office2007Form
    {
        CZCM zcm = new CZCM();
        public frmZCMManage()
        {
            InitializeComponent();

            this.textBox1.Text = zcm.testGetJQM();

            //默认值是，无
            this.textBox2.Text = Ctrl_MainData.getUserInfoInstance().getFilePath(FilePathType.ZCM);

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string strZCM = zcm.returnZCM(textBox1.Text.ToString());
            if (strZCM == textBox2.Text.ToString().Trim())
            {
                Ctrl_MainData.getUserInfoInstance().UpdateFilePath(FilePathType.ZCM, strZCM);
            }
            else
            {
                MessageBox.Show("您输入的注册码有误，不能通过验证！");
            }
        }
    }
}
