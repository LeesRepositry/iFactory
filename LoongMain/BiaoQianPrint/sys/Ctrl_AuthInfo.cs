﻿using System;
using System.Collections.Generic;

using System.Text;

using System.IO;
using BiaoQianPrint.Tools;
using System.Runtime.Serialization.Formatters.Binary;
//就像一个权限管理员。  定义一个我的对象，然后一直持有我。
//     我提供几个方法，可以知道某个人是否有权限。告诉我需要加载数据，
//  修改了权限数据后，别忘了告诉我要保存数据。

//我不会把 ListAuth<Auth> 提高给别人，防止别人胡乱改。只能通过我的Public的接口访问权限。
namespace BiaoQianPrint.sys
{
    class Mdl_AuthItem
    {
        public string mAuthID { set; get; }//主键
        public string mAuthName { set; get; }
        public string mFID { set; get; }
    }

    class Ctrl_AuthInfo
    {
        private string _FilePath_bin=System.Windows.Forms.Application.StartupPath+"\\Ai.bin";
        private string _FilePath_txt = System.Windows.Forms.Application.StartupPath + "\\Ai.txt";
        private List<Mdl_AuthItem> _listAuth = new List<Mdl_AuthItem>();

        #region 单例模式
        private static Ctrl_AuthInfo instance;
        public static Ctrl_AuthInfo getUserInfoInstance()
        {
            if (instance == null)
            {
                instance = new Ctrl_AuthInfo();
            }
            return instance;
        }
        #endregion

        #region 读写数据接口。
        private void ReadData()
        {
            //_listAuth = FileDbManager.ReadData<Mdl_AuthItem>(_FilePath_txt);
            _listAuth = FileDbManager.ReadDataFromBFile<Mdl_AuthItem>(_FilePath_bin);
        }
        private void SaveData()
        {
            //FileDbManager.SaveData<Mdl_AuthItem>(_listAuth, _FilePath_txt);
            FileDbManager.SaveDataToBFile<Mdl_AuthItem>(_listAuth, _FilePath_bin);
        }
        #endregion

        public Ctrl_AuthInfo()
        {
            this.ReadData();
            //本界面没有 新建等维护界面。 在txt里面进行维护，然后读取。正式运行后就使用序列化的方式了。
            //SaveData();
        }

        // 返回所有的 Auth数据
        public List<Mdl_AuthItem> GetAllAuth()
        {
            return this._listAuth;
        }
        public List<Mdl_AuthItem> GetAllAuthByFid(string Fid)
        {
            List<Mdl_AuthItem> listResult = new List<Mdl_AuthItem>();
            foreach(Mdl_AuthItem item in _listAuth){
                if(item.mFID==Fid)
                    listResult.Add(item);
            }
            return listResult;
        }
        
    }
}
