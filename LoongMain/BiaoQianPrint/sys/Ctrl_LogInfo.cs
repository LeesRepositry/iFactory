﻿using System;
using System.Collections.Generic;

using System.Text;

namespace BiaoQianPrint.Model
{
    class Mdl_LogInfoItem
    {
        //public int mAutoID;  //主键
        public string mUserID;
        public string mUserName;
        public string mOperationTime;
        public string mOperationContent;
        public string mRemark;

        public Mdl_LogInfoItem(string pUserID, string pUserName, string pOperationTime, string pOperationContent, string pRemark)
        {
            mUserID = pUserID;
            mUserName = pUserName;
            mOperationTime = pOperationTime;
            mOperationContent = pOperationContent;
            mRemark = pRemark;
        }
    }

    class Mod_LogInfo
    {
        private List<Mdl_LogInfoItem> _listLogInfoItem = new List<Mdl_LogInfoItem>();

        public void InsertOneRow(string pUserID,string pUserName,string pOperationTime,string pOperationContent,string pRemark)
        {
            Mdl_LogInfoItem mItem = new Mdl_LogInfoItem(pUserID, pUserName, pOperationTime, pOperationContent, pRemark);
            _listLogInfoItem.Add(mItem);
        }
    }
}
