﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using BiaoQianPrint.Tools;
using System.Windows.Forms;
namespace BiaoQianPrint.Model
{
    [Serializable]
    public class Mdl_UserRow
    {
        public string UserID{get; set;}
        public string RoleID { get; set; }
        public string UserLogID { get; set; }//登录时账号，
        public string UserName { get; set; }  //员工姓名
        public string Password { get; set; }     
        public int UserStatus { get; set; }          //用户状态。
        public string ChangePasswordTime { get; set; }   //密码修改时间
        public string CreateUserName { get; set; }   //创建人名称
        public string CreateTime { get; set; }           //创建时间
        public string EditUserName { get; set; }        //修改人名称
        public string EditTime { get; set; }             //修改时间

        public Mdl_UserRow() { }
    }
    class Mod_UserInfo
    {
        private string _FilePath=System.Windows.Forms.Application.StartupPath+"\\Ui.bin";
        private List<Mdl_UserRow> _listUser = new List<Mdl_UserRow>();

        #region 单例模式
        private static Mod_UserInfo instance;
        public static Mod_UserInfo getUserInfoInstance()
        {
            if (instance == null)
            {
                instance = new Mod_UserInfo();
            }
            return instance;
        }
        #endregion

        #region 本地经常在2种 数据格式间切换，要挨着找所有的地方，因此集中在这儿。这是个接口。
        private void ReadData()
        {
            _listUser = FileDbManager.ReadDataFromBFile<Mdl_UserRow>(_FilePath);
        }
        private void SaveData()
        {
            FileDbManager.SaveDataToBFile(_listUser, _FilePath);
        }
        #endregion

        public Mod_UserInfo()
        {
            ReadData(); 
        }
        
        #region  增删改
        // RoleID,UserLogID,UserName,必须输入。
        public bool AddNewUser(Mdl_UserRow item)
        {
            //验证：新增的UserLogID 不能重复。
            foreach (Mdl_UserRow row in _listUser)
            {
                if (row.UserLogID == item.UserLogID)
                {
                    MessageBox.Show("登录帐号已经存在，请换个账号！在保存。");
                    return false;
                }
            }

            Mdl_UserRow newItem = new Mdl_UserRow();
            newItem.UserID = CommonClass.getNewID<Mdl_UserRow>(_listUser, "UserID");
            newItem.RoleID=item.RoleID;
            newItem.UserLogID = item.UserLogID;
            newItem.UserName=item.UserName;
            newItem.Password=CommonClass.GetMD5("8888");
            newItem.UserStatus=0;
            newItem.ChangePasswordTime = CommonClass.GetSysTime().ToString("yyyy-MM-dd hh:mm");
            newItem.CreateUserName = CommonClass.mCurrentUser.UserName+"";
            newItem.CreateTime = newItem.ChangePasswordTime;
            newItem.EditUserName="";
            newItem.EditTime=newItem.ChangePasswordTime;
            this._listUser.Add(newItem);
            SaveData();
            return true;
        }
        //  RoleID，UserName 可以改
        public bool UpdateUser(Mdl_UserRow pItem)
        {
            foreach (Mdl_UserRow item in _listUser)
            {
                if (item.UserID == pItem.UserID)
                {
                    item.RoleID = pItem.RoleID;
                    item.UserName = pItem.UserName;
                    item.EditTime = CommonClass.GetSysTime().ToString("yyyy-MM-dd hh:mm");
                    item.EditUserName = CommonClass.mCurrentUser.UserName;
                    SaveData();
                    return true;
                }
            }
            MessageBox.Show("没有找到此账号！");
            return false;
        }

        public bool ClearPassword(string pUserID, string pPassword)
        {
            foreach (Mdl_UserRow item in _listUser)
            {
                if (item.UserID == pUserID)
                {
                    item.Password = CommonClass.GetMD5(pPassword);
                    item.ChangePasswordTime = CommonClass.GetSysTime().ToString("yyyy-MM-dd hh:mm");
                    SaveData();
                    return true;
                }
            }
            MessageBox.Show("没有找到此账号！");
            return false;
        }
        public bool DelByUserID(string pUserID)
        {
            foreach (Mdl_UserRow item in _listUser)
            {
                if (item.UserID == pUserID)
                {
                    _listUser.Remove(item);
                    SaveData();
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region 审核账号
        public bool QiyongUser(string pUserID)
        {
            foreach (Mdl_UserRow item in _listUser)
            {
                if (item.UserID == pUserID)
                {
                    item.UserStatus = 1;
                    SaveData();
                    return true;
                }
            }
            return false;
        }
        public bool DongjieUser(string pUserID)
        {
            foreach (Mdl_UserRow item in _listUser)
            {
                if (item.UserID == pUserID)
                {
                    item.UserStatus = 0;
                    SaveData();
                    return true;
                }
            }
            return false;
        }
        #endregion

        // 找不到是，返回Null
        public Mdl_UserRow GetUserByUserID(string pUserLogID)
        {
            Mdl_UserRow mUser = new Mdl_UserRow();
            foreach (Mdl_UserRow item in _listUser) 
            {
                if (item.UserLogID == pUserLogID)
                {
                    mUser = item;
                    return mUser;
                }
            }
            MessageBox.Show("找不到此用户！UserID：" + pUserLogID);
            return null;
        }

        // 返回所有的 User数据
        public List<Mdl_UserRow> GetAllUser()
        {
            return this._listUser;
        }
    }
}
