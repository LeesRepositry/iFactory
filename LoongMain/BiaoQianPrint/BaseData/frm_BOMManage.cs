﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevComponents.DotNetBar;
namespace BiaoQianPrint.BaseData
{
    public partial class frm_BOMManage : Office2007Form
    {
        public frm_BOMManage()
        {
            InitializeComponent();
        }

        private void frm_BOMManage_Resize(object sender, EventArgs e)
        {
            dgvDetl.Width = this.Width - 10;
            dgvDetl.Height = this.Height - dgvDetl.Top - 10;
        }

        private void linkImpoortExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmImportExcel_Bom frm = new frmImportExcel_Bom();
            frm.ShowDialog();
        }
    }
}
