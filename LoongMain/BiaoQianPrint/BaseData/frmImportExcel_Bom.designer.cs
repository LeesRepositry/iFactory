﻿namespace BiaoQianPrint.BaseData
{
    partial class frmImportExcel_Bom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.btnExport = new System.Windows.Forms.Button();
            this.cblList = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblRowCount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnToDB = new System.Windows.Forms.Button();
            this.grd = new System.Windows.Forms.DataGridView();
            this.btnDaoRu = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.filePathBox = new System.Windows.Forms.TextBox();
            this.lblWaiBom = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblZhongBom = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblNeiBom = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDlg
            // 
            this.openFileDlg.FileName = "openFileDialog1";
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(512, 40);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(109, 37);
            this.btnExport.TabIndex = 45;
            this.btnExport.Text = "导出到Excel";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // cblList
            // 
            this.cblList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cblList.FormattingEnabled = true;
            this.cblList.Location = new System.Drawing.Point(512, 10);
            this.cblList.Name = "cblList";
            this.cblList.Size = new System.Drawing.Size(176, 20);
            this.cblList.TabIndex = 44;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox1.ForeColor = System.Drawing.Color.Purple;
            this.textBox1.Location = new System.Drawing.Point(6, 82);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(955, 71);
            this.textBox1.TabIndex = 43;
            this.textBox1.Text = "【1】导入的Excel列全部设置为文本\r\n【2】第一列PN；第二列内BOM；第三列中BOM；第四列外BOM；最后一列是页数\r\n【3】导入完成后核对一下右上角的合计" +
    "。";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(775, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 42;
            this.label2.Text = "请核对共计：";
            // 
            // lblRowCount
            // 
            this.lblRowCount.AutoSize = true;
            this.lblRowCount.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblRowCount.Location = new System.Drawing.Point(885, 13);
            this.lblRowCount.Name = "lblRowCount";
            this.lblRowCount.Size = new System.Drawing.Size(11, 12);
            this.lblRowCount.TabIndex = 39;
            this.lblRowCount.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(852, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 38;
            this.label1.Text = "行数";
            // 
            // btnToDB
            // 
            this.btnToDB.Location = new System.Drawing.Point(12, 43);
            this.btnToDB.Name = "btnToDB";
            this.btnToDB.Size = new System.Drawing.Size(122, 34);
            this.btnToDB.TabIndex = 37;
            this.btnToDB.Text = "保存表格中全部数据";
            this.btnToDB.UseVisualStyleBackColor = true;
            this.btnToDB.Click += new System.EventHandler(this.btnToDB_Click);
            // 
            // grd
            // 
            this.grd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd.Location = new System.Drawing.Point(-1, 159);
            this.grd.Name = "grd";
            this.grd.RowTemplate.Height = 23;
            this.grd.Size = new System.Drawing.Size(1104, 215);
            this.grd.TabIndex = 36;
            // 
            // btnDaoRu
            // 
            this.btnDaoRu.Location = new System.Drawing.Point(694, 4);
            this.btnDaoRu.Name = "btnDaoRu";
            this.btnDaoRu.Size = new System.Drawing.Size(75, 34);
            this.btnDaoRu.TabIndex = 35;
            this.btnDaoRu.Text = "读取数据";
            this.btnDaoRu.UseVisualStyleBackColor = true;
            this.btnDaoRu.Click += new System.EventHandler(this.btnDaoRu_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(8, 4);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 34);
            this.btnOpen.TabIndex = 34;
            this.btnOpen.Text = "选择文件";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // filePathBox
            // 
            this.filePathBox.Location = new System.Drawing.Point(89, 10);
            this.filePathBox.Name = "filePathBox";
            this.filePathBox.Size = new System.Drawing.Size(417, 21);
            this.filePathBox.TabIndex = 33;
            // 
            // lblWaiBom
            // 
            this.lblWaiBom.AutoSize = true;
            this.lblWaiBom.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblWaiBom.Location = new System.Drawing.Point(1016, 59);
            this.lblWaiBom.Name = "lblWaiBom";
            this.lblWaiBom.Size = new System.Drawing.Size(11, 12);
            this.lblWaiBom.TabIndex = 51;
            this.lblWaiBom.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(935, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 12);
            this.label7.TabIndex = 50;
            this.label7.Text = "外BOM数量";
            // 
            // lblZhongBom
            // 
            this.lblZhongBom.AutoSize = true;
            this.lblZhongBom.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblZhongBom.Location = new System.Drawing.Point(1016, 40);
            this.lblZhongBom.Name = "lblZhongBom";
            this.lblZhongBom.Size = new System.Drawing.Size(11, 12);
            this.lblZhongBom.TabIndex = 49;
            this.lblZhongBom.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(935, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 48;
            this.label5.Text = "中BOM数量";
            // 
            // lblNeiBom
            // 
            this.lblNeiBom.AutoSize = true;
            this.lblNeiBom.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblNeiBom.Location = new System.Drawing.Point(1016, 19);
            this.lblNeiBom.Name = "lblNeiBom";
            this.lblNeiBom.Size = new System.Drawing.Size(11, 12);
            this.lblNeiBom.TabIndex = 47;
            this.lblNeiBom.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(935, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 46;
            this.label3.Text = "内BOM数量";
            // 
            // FrmImportExcelBZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1103, 375);
            this.Controls.Add(this.lblWaiBom);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblZhongBom);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblNeiBom);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.cblList);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblRowCount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnToDB);
            this.Controls.Add(this.grd);
            this.Controls.Add(this.btnDaoRu);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.filePathBox);
            this.Name = "FrmImportExcelBZ";
            this.Text = "FrmImportExcelBZ";
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDlg;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.ComboBox cblList;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblRowCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnToDB;
        private System.Windows.Forms.DataGridView grd;
        private System.Windows.Forms.Button btnDaoRu;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.TextBox filePathBox;
        private System.Windows.Forms.Label lblWaiBom;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblZhongBom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblNeiBom;
        private System.Windows.Forms.Label label3;
    }
}