﻿namespace BiaoQianPrint.BaseData
{
    partial class frm_BOMManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.txtPN = new System.Windows.Forms.TextBox();
            this.btnDetl_Refresh = new System.Windows.Forms.Button();
            this.lblHangShu = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblDetlHeJi = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvDetl = new System.Windows.Forms.DataGridView();
            this.linkImpoortExcel = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetl)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 15;
            this.label3.Text = "PN号：";
            // 
            // txtPN
            // 
            this.txtPN.Location = new System.Drawing.Point(86, 12);
            this.txtPN.Name = "txtPN";
            this.txtPN.Size = new System.Drawing.Size(113, 21);
            this.txtPN.TabIndex = 16;
            // 
            // btnDetl_Refresh
            // 
            this.btnDetl_Refresh.Location = new System.Drawing.Point(243, 10);
            this.btnDetl_Refresh.Name = "btnDetl_Refresh";
            this.btnDetl_Refresh.Size = new System.Drawing.Size(65, 28);
            this.btnDetl_Refresh.TabIndex = 17;
            this.btnDetl_Refresh.Text = "刷 新";
            this.btnDetl_Refresh.UseVisualStyleBackColor = true;
            // 
            // lblHangShu
            // 
            this.lblHangShu.AutoSize = true;
            this.lblHangShu.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblHangShu.ForeColor = System.Drawing.Color.Maroon;
            this.lblHangShu.Location = new System.Drawing.Point(392, 17);
            this.lblHangShu.Name = "lblHangShu";
            this.lblHangShu.Size = new System.Drawing.Size(15, 14);
            this.lblHangShu.TabIndex = 24;
            this.lblHangShu.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.Maroon;
            this.label6.Location = new System.Drawing.Point(448, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 14);
            this.label6.TabIndex = 23;
            this.label6.Text = "数量合计：";
            // 
            // lblDetlHeJi
            // 
            this.lblDetlHeJi.AutoSize = true;
            this.lblDetlHeJi.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblDetlHeJi.ForeColor = System.Drawing.Color.Maroon;
            this.lblDetlHeJi.Location = new System.Drawing.Point(537, 17);
            this.lblDetlHeJi.Name = "lblDetlHeJi";
            this.lblDetlHeJi.Size = new System.Drawing.Size(15, 14);
            this.lblDetlHeJi.TabIndex = 22;
            this.lblDetlHeJi.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(332, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 14);
            this.label5.TabIndex = 21;
            this.label5.Text = "行数：";
            // 
            // dgvDetl
            // 
            this.dgvDetl.AllowUserToAddRows = false;
            this.dgvDetl.AllowUserToDeleteRows = false;
            this.dgvDetl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetl.Location = new System.Drawing.Point(4, 61);
            this.dgvDetl.Name = "dgvDetl";
            this.dgvDetl.ReadOnly = true;
            this.dgvDetl.RowTemplate.Height = 23;
            this.dgvDetl.Size = new System.Drawing.Size(732, 264);
            this.dgvDetl.TabIndex = 20;
            // 
            // linkImpoortExcel
            // 
            this.linkImpoortExcel.AutoSize = true;
            this.linkImpoortExcel.Location = new System.Drawing.Point(609, 21);
            this.linkImpoortExcel.Name = "linkImpoortExcel";
            this.linkImpoortExcel.Size = new System.Drawing.Size(59, 12);
            this.linkImpoortExcel.TabIndex = 25;
            this.linkImpoortExcel.TabStop = true;
            this.linkImpoortExcel.Text = "导入新BOM";
            this.linkImpoortExcel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkImpoortExcel_LinkClicked);
            // 
            // frm_BOMManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 355);
            this.Controls.Add(this.linkImpoortExcel);
            this.Controls.Add(this.lblHangShu);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblDetlHeJi);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dgvDetl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPN);
            this.Controls.Add(this.btnDetl_Refresh);
            this.DoubleBuffered = true;
            this.Name = "frm_BOMManage";
            this.Text = "包装BOM管理";
            this.Resize += new System.EventHandler(this.frm_BOMManage_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPN;
        private System.Windows.Forms.Button btnDetl_Refresh;
        private System.Windows.Forms.Label lblHangShu;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblDetlHeJi;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgvDetl;
        private System.Windows.Forms.LinkLabel linkImpoortExcel;
    }
}