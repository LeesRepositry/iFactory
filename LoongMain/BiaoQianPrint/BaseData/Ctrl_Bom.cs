﻿using System;
using System.Collections.Generic;
using System.Text;

using BiaoQianPrint.Tools;
using BiaoQianPrint.YeWu;
namespace BiaoQianPrint.BaseData
{
    #region Mdl_BomRow
    class Mdl_BomRow
    {
        public string mPN { get; set; }
        public int mNeiNum { get; set; }
        public int mZhongNum { get; set; }
        public int mWaiNum { get; set; }
        public int mYeShu { get; set; }
    }
    #endregion  

    //一个对象，对应一个Bom文件。
    //可以是写入数据到Bom文件，或者从Bom文件读取数据
    class Ctrl_Bom
    {
        private string _FileName="";
        public List<Mdl_BomRow> _listData = new List<Mdl_BomRow>();
        //private double ShSum;


        #region 读写数据接口。
        private void ReadData()
        {
            //_listAuth = FileDbManager.ReadData<Mdl_AuthItem>(_FilePath_txt);
            _listData = FileDbManager.ReadData<Mdl_BomRow>(CommonClass.BOMDBpath + "\\" +_FileName);
        }
        private void SaveData()
        {
            FileDbManager.SaveData<Mdl_BomRow>(_listData, CommonClass.BOMDBpath + "\\" + _FileName);
            //FileDbManager.SaveDataToBFile<Mdl_AuthItem>(_listAuth, _FilePath_bin);
        }
        #endregion


        //读取数据时，使用这个
        public Ctrl_Bom(string pFileName)
        {
            _FileName = pFileName;
            ReadData();
        }

        public void AddItem(string pPN, int pNeiNum, int pZhongNum, int pWaiNum,int pYeShu)
        {

            Mdl_BomRow item = new Mdl_BomRow();
            item.mPN = pPN;
            item.mNeiNum = pNeiNum;
            item.mZhongNum = pZhongNum;
            item.mWaiNum = pWaiNum;
            item.mYeShu = pYeShu;

            //SHDate = pSHDate;
            //ShSum += pNum;

            _listData.Add(item);
        }
        public void SaveDataToFile()
        {
            //1、写到txt文件里面
            _FileName ="BOM_" + CommonClass.GetSysTime().ToString("yyyy-MM-dd_hh-mm-ss") + 
                "_"+ _listData.Count.ToString() + ".txt";
            SaveData();
            Ctrl_MainData.getUserInfoInstance().UpdateFilePath(FilePathType.BOMFilePath, _FileName);
        }

        //根据PN号，返回 三个BOM数量
        public void getBomNumByPN(string pPN,out int pNeiBom,out int pZhongBom,out int pWaiBom)
        {
            pNeiBom = 0;
            pZhongBom = 0;
            pWaiBom = 0;

            foreach (Mdl_BomRow itemBom in _listData)
            {
                //Bom中的PN号必须不能重复的。
                if (itemBom.mPN == pPN)
                {
                    pNeiBom = itemBom.mNeiNum;
                    pZhongBom = itemBom.mZhongNum;
                    pWaiBom = itemBom.mWaiNum;
                    break;
                }
            }
        }
    }
}
