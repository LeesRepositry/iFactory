﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Collections;
using System.Reflection;

using BiaoQianPrint.Excel;
using BiaoQianPrint.Model;
using BiaoQianPrint.Tools;
using BiaoQianPrint.YeWu;
namespace BiaoQianPrint.BaseData
{
    public partial class frmImportExcel_Bom : Form
    {
        DataTable dt;
        IExcelImport excelManager = new ExcelImportImpl();

        public frmImportExcel_Bom()
        {
            InitializeComponent();
        }

        #region 打开文件并读取Sheet
        private void btnOpen_Click(object sender, EventArgs e)
        {
            openFileDlg.Filter = "Excel2007(*.xlsx)|*.xlsx|Excel2003(*.xls)|*.xls";
            if (openFileDlg.ShowDialog() == DialogResult.OK)
            {
                filePathBox.Text = openFileDlg.FileName;
                DataTable dtSheets = excelManager.GetSheetList(filePathBox.Text.ToString());
                //GetSheetList(filePathBox.Text.ToString());

                cblList.Items.Clear();
                foreach (DataRow dr in dtSheets.Rows)
                {
                    // 用下面方法无效的sheet
                    if (dr[2].ToString().Contains("$") && !dr[2].ToString().EndsWith("$"))
                    {
                        continue;
                    }
                    cblList.Items.Add(dr[2].ToString());
                }
                if (cblList.Items.Count > 0) cblList.SelectedIndex = 0;
            }
        }
        #endregion

        #region 读取数据，并合计
        private void btnDaoRu_Click(object sender, EventArgs e)
        {
            string strSheetName = cblList.SelectedItem.ToString();
            dt = excelManager.getDataTableFromExcel(strSheetName);
            SumInfo();
            grd.DataSource = dt;
        } 
        #endregion

        #region SumInfo()
        private void SumInfo()
        {
            int mCount = 0;
            double mSumNei = 0;
            double mSumZhong = 0;
            double mSumWai = 0;
            int tmpNum = 0;

            foreach (DataRow row in dt.Rows)
            {
                if (string.IsNullOrEmpty(row[0].ToString().Trim()) == false)
                {
                    mCount++;
                    
                    int.TryParse( row[1].ToString(), out tmpNum);
                    mSumNei = mSumNei + tmpNum;

                    int.TryParse(row[2].ToString(), out tmpNum);
                    mSumZhong = mSumZhong + tmpNum;

                    int.TryParse(row[3].ToString(), out tmpNum);                    
                    mSumWai = mSumWai + tmpNum;
                }
            }
            lblRowCount.Text = mCount.ToString();
            lblNeiBom.Text = mSumNei.ToString();
            lblZhongBom.Text = mSumZhong.ToString();
            lblWaiBom.Text = mSumWai.ToString();
        }
        #endregion

        private void btnToDB_Click(object sender, EventArgs e)
        {
            
            System.Text.StringBuilder sb = new StringBuilder();

            if (dt != null)
            {
                Ctrl_Bom objBom=new Ctrl_Bom("");
                foreach (DataRow row in dt.Rows)
                {
                    //然后一行行的把数据写入到 txt文件里面。
                    if (string.IsNullOrEmpty(row[0].ToString().Trim()) == false)
                    {
                        int mNeiNum=CommonClass.GetIntFromString(row[1].ToString());
                        int mZhongNum=CommonClass.GetIntFromString(row[2].ToString());
                        int mWaiNum=CommonClass.GetIntFromString(row[3].ToString());
                        int mYeShu=CommonClass.GetIntFromString(row[4].ToString());
                        objBom.AddItem(row[0].ToString(), mNeiNum, mZhongNum, mWaiNum, mYeShu);
                        if (string.IsNullOrEmpty(row[0].ToString().Trim()) == true)
                        {
                            sb.AppendLine(row["PN"].ToString() + "存在DC空的情况！\r\n");
                        }
                    }
                }
                if (sb.Length > 0)
                {
                    MessageBox.Show(sb.ToString());
                    MessageBox.Show("请改正后重新导入！");
                }
                else
                {
                    objBom.SaveDataToFile();
                }
            }
        }

        #region 导出文件
        private void btnExport_Click(object sender, EventArgs e)
        {
            string saveFileName = "";
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.DefaultExt = "xlsx|xls";
            saveDialog.Filter = "Excel(*.xlsx)|*.xlsx|Excel(*.xls)|*.xls";
      
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                saveFileName = saveDialog.FileName;
                IExcelExport excelManager = new ExcelExportImpl();
                string stringErr = excelManager.ExportExcelMaster(dt, saveFileName);
                #region 打开保存成功的文件
                try
                {
                    System.Diagnostics.Process.Start(saveFileName);//打开导出的文件

                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
                #endregion
            }
        }
       
        #endregion
    }
}
