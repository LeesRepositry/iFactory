﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BiaoQianPrint.Excel
{
    interface IExcelExport
    {
        string ExportExcelMaster(System.Data.DataTable dt, string saveFileName);
    }
}
