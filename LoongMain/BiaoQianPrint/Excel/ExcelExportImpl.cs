﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Reflection;
namespace BiaoQianPrint.Excel
{
    class ExcelExportImpl:IExcelExport
    {
        /// <summary>         
        /// excel format compatiable         
        /// </summary>         
        /// <param name="ds"></param>         
        /// <param name="saveFileName">output file name with the physical path</param>         
        /// <param name="errorInfo"></param>         
        public string ExportExcelMaster(System.Data.DataTable dt, string saveFileName)
        {
            object objApp;
            object objBook;
            object objBooks;
            object objSheets;
            object objSheet;
            object objCells;
            object[] Parameters;
            string errorInfo = "";
            try
            {                 // 获取Excel类型并建立其实例                 
                Type objExcelType = Type.GetTypeFromProgID("Excel.Application");
                if (objExcelType == null)
                {
                    errorInfo = "Excel does not installed!";
                    return errorInfo;
                }
                objApp = Activator.CreateInstance(objExcelType);
                if (objApp == null)
                {
                    errorInfo = "Excel can not be created on this pc!";
                    return errorInfo;
                }
                //获取Workbook集                
                objBooks = objApp.GetType().InvokeMember("Workbooks", BindingFlags.GetProperty, null, objApp, null);
                //添加一个新的Workbook                 
                objBook = objBooks.GetType().InvokeMember("Add", BindingFlags.InvokeMethod, null, objBooks, null);
                //获取Sheet集                 
                objSheets = objBook.GetType().InvokeMember("Worksheets", BindingFlags.GetProperty, null, objBook, null);
                //获取第一个Sheet对象                
                Parameters = new Object[1] { 1 };
                objSheet = objSheets.GetType().InvokeMember("Item", BindingFlags.GetProperty, null, objSheets, Parameters);
                try
                {
                    //写入字段                      
                    for (int n = 0; n < dt.Columns.Count; n++)
                    {
                        Parameters = new Object[2] { 1, n + 1 };
                        objCells = objSheet.GetType().InvokeMember("Cells", BindingFlags.GetProperty, null, objSheet, Parameters);
                        //向指定单元格填写内容值                       
                        Parameters = new Object[1] { dt.Columns[n].ColumnName };
                        objCells.GetType().InvokeMember("Value", BindingFlags.SetProperty, null, objCells, Parameters);
                    }
                    //获取操作范围               
                    for (int r = 1; r <= dt.Rows.Count; r++)
                    {
                        for (int i = 1; i <= dt.Columns.Count; i++)
                        {
                            Parameters = new Object[2] { r + 1, i };
                            objCells = objSheet.GetType().InvokeMember("Cells", BindingFlags.GetProperty, null, objSheet, Parameters);
                            //向指定单元格填写内容值                            
                            Parameters = new Object[1] { dt.Rows[r - 1][i - 1].ToString() };
                            objCells.GetType().InvokeMember("Value", BindingFlags.SetProperty, null, objCells, Parameters);
                        }
                    }
                }
                catch (Exception operException)
                {
                    //MessageBox.Show(operException.Message);           
                    throw;
                }
                finally
                {
                    //不提示保存     
                    Parameters = new Object[1] { false };
                    objApp.GetType().InvokeMember("DisplayAlerts", BindingFlags.SetProperty, null, objApp, Parameters);
                    //保存文件并退出                    
                    Parameters = new Object[1] { saveFileName };
                    objBook.GetType().InvokeMember("SaveAs", BindingFlags.InvokeMethod, null, objBook, Parameters);
                    objApp.GetType().InvokeMember("Quit", BindingFlags.InvokeMethod, null, objApp, null);
                    GC.Collect();
                }
                if (System.IO.File.Exists(saveFileName)) System.Diagnostics.Process.Start(saveFileName);
                //打开EXCEL           
            }
            catch (Exception theException)
            {                // String errorMessage;             
                errorInfo = theException.Message;
                //MessageBox.Show(errorMessage, "Error");              
                throw;
            }
            return "";
        }
    }
}
