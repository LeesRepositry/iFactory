﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;

namespace BiaoQianPrint.Excel
{
    //必须先调用第一个，  把FilePath赋值后，再调用第二个。
    interface IExcelImport
    {
        DataTable GetSheetList(string strPath);
        
        DataTable getDataTableFromExcel(string pSheetName);
        
    }
}
