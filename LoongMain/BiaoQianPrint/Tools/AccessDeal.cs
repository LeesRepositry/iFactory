using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;

namespace BiaoQianPrint.Tools
{
    public static class SqlDeal
    {
        //变量声明处#region 变量声明处   
        public static OleDbConnection Conn;
        public static string ConnString;//连接字符串 
        public static string Dbpath = Application.StartupPath + @"\KMSDB.mdb";

        public static bool CreateSqlCon()   
        {   
            try
            {
                ConnString = "Provider=Microsoft.Jet.OleDb.4.0;Data Source=";
                ConnString += Dbpath;
                //ConnString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Dbpath + ";Persist Security Info=False;";
                Conn = new OleDbConnection(ConnString);
                Conn.Open();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("打开数据库错误！错误描述：" + ex.Message);
                return false;
            }
            return true;
        }   
        //关闭数据库连接。
        public static void CloseSqlCon()   
        {   
            Conn.Close();   
        }  
     
        // 根据SQL命令返回数据DataTable数据表。
        public static DataTable GetSqlDataTable(string SQL)   
        {
            DataTable Dt = new DataTable();
            try
            {
                OleDbDataAdapter adapter = new OleDbDataAdapter();
                OleDbCommand command = new OleDbCommand(SQL, Conn);
                adapter.SelectCommand = command;
                adapter.Fill(Dt);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("返回SQL数据表出错！错误描述：" + ex.Message);
            }
            return Dt;  
        }   
      
 
        public static string GetSqlReaderResult(string SQL)   
        {
            DataTable Dt = new DataTable();
            string mResult = "";

            OleDbDataAdapter adapter = new OleDbDataAdapter();   
            OleDbCommand command = new OleDbCommand(SQL, Conn);   
            adapter.SelectCommand = command;
            adapter.Fill(Dt);


            if (Dt.Rows.Count>0)
            {
                mResult = Dt.Rows[0][0].ToString();
                adapter.Dispose();
                return mResult;
            }
            else
            {
                mResult = "";
                adapter.Dispose();
                return mResult;
            }
        }   
   
        /**//// <summary>   
        /// 执行SQL命令，不需要返回数据的修改，删除可以使用本函数   
        /// </summary>   
        /// <param name="SQL"></param>   
        /// <returns></returns>   
        public static bool ExecuteSQLReturn(string SQL)   
        {   
            OleDbCommand cmd = new OleDbCommand(SQL, Conn);   
            try 
            {   
                cmd.ExecuteNonQuery();   
                return true;   
            }   
            catch(Exception ex)
            {
                MessageBox.Show("执行SQL语句出错，错误描述：" + ex.Message);
                return false;   
            }
        }
        public static string CreateID(string mID, string mInfo, string mPreStr, int mNoLen)
        {
            //获取当前最大编号
            string strSQL = "select MAX(" + mID + ") from " + mInfo + " where LEFT(" + mID + "," + mPreStr.Length + ")='" + mPreStr + "'";
            string strtmp = GetSqlReaderResult(strSQL);
            //转换为数字，并+1
            strtmp = strtmp.Replace(mPreStr, "");
            int inttmp = 0;
            int.TryParse(strtmp, out inttmp);
            inttmp++;
            strtmp = inttmp.ToString();
            //循环前面加字符‘0’ 来补足位数，
            while (strtmp.Length < mNoLen)
            {
                strtmp = "0" + strtmp;
            }
            //返回合适的值
            return mPreStr + strtmp;
        }
  
    }
}
