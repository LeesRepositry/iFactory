﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace XinHaiOA
{
    public partial class frmOneSelect : Form
    {
        private int mPreNum = -1;
        public string strSQL = "";
        public string mID = "";
        public string mName = "";

        private Datadeal mDD = new Datadeal();
        public frmOneSelect()
        {
            InitializeComponent();
        }

        private void frmOneSelect_Load(object sender, EventArgs e)
        {
            lvw.GridLines = true;//显示各个记录的分隔线 
            lvw.FullRowSelect = true;//要选择就是一行 
            lvw.View = View.Details;//定义列表显示的方式 
            lvw.Scrollable = true;//需要时候显示滚动条 
            lvw.MultiSelect = false; // 不可以多行选择 
            lvw.HeaderStyle = ColumnHeaderStyle.Nonclickable;
            // 针对数据库的字段名称，建立与之适应显示表头 
            lvw.Columns.Add("序号", 30, HorizontalAlignment.Left);
            lvw.Columns.Add("名称", 220, HorizontalAlignment.Left);
            lvw.Columns.Add("编号", 60, HorizontalAlignment.Left);
            if (strSQL != "")
            {
                DataTable dt = mDD.GetSqlSet(strSQL).Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ListViewItem li = new ListViewItem();
                    li.SubItems.Clear();
                    li.SubItems[0].Text = i.ToString();
                    li.SubItems.Add (dt.Rows[i][1].ToString());
                    li.SubItems.Add(dt.Rows[i][0].ToString());
                    lvw.Items.Add(li); 
                }
            }
        }

        private void lvw_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (lvw.SelectedItems != null)
            {
                int i = lvw.SelectedIndices[0];
                mID = lvw.Items[i].SubItems[2].Text.ToString();
                mName = lvw.Items[i].SubItems[1].Text.ToString();
                this.DialogResult = DialogResult.OK;                
            }
        }

        private void lvw_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                if (lvw.SelectedItems.Count >0)
                {
                    int i = lvw.SelectedIndices[0];
                    mID = lvw.Items[i].SubItems[2].Text.ToString();
                    mName = lvw.Items[i].SubItems[1].Text.ToString();
                    this.DialogResult = DialogResult.OK;
                }
            }
            if (e.KeyValue == 39)
            {
                btnCancel.Focus();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void lvw_KeyPress(object sender, KeyPressEventArgs e)
        {
            //增加 使用数字 快速定位。  NB 不可能输入 大于10的数。
            int mRow=0;
            string mTmp = "";
            /*if(IsInt(e.KeyChar.ToString()))
            {
                mTmp = e.KeyChar.ToString();
                mRow =Convert.ToInt16(mTmp);
            }            
            if (mPreNum == -1)
            {//定位到 mRow    
                ListViewItem liv = new ListViewItem();
                liv = this.lvw.Items[mRow];                    
                liv.Selected = true;
                mPreNum = mRow;                
            }
            else
            {//定位到 mPreNum+Row

            } */                                
        }
        private static bool IsInt(string inString)
        {
            try
            {
                double var1 = Convert.ToDouble(inString);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}