﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;
namespace BiaoQianPrint.Tools
{
    class DGVHelper
    {

        public static void initDGVbyString(string strList,ref DataGridView DGB)
        {
            string[] temColList = strList.Split('|');
            //先初始化Grid                        
            for (int I = 0; I < temColList.Length; I++)
            {
                //设置标题文字 
                System.Windows.Forms.DataGridViewTextBoxColumn Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
                Column1.HeaderText = temColList[I];
                Column1.Name = "Column1";
                DGB.Columns.Add(Column1);
                //设置列宽 
                DGB.Columns[I].Width = 120;
                DGB.Columns[I].ReadOnly = true;
            }
        }
    }
}
