using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Windows.Forms;
namespace BiaoQianPrint.Tools
{
    public class FileManager
    {
        #region  打开文件，并读取文件到ListStr
        public static List<string> fGetListStrFromFile()
        {
            FileStream fs = null; //声明文件流的对象  
            StreamReader sr = null; //声明读取器的对象  

            List<string> listStr = new List<string>();

            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Multiselect = false; //不允许多选文件  
            openFileDialog1.Filter = "备份文件|*.txt"; //文件类型  
            string path = null;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                path = openFileDialog1.FileName;
                if (path.Equals(null) || path.Equals(""))
                {
                    MessageBox.Show("请选择文件");
                    return listStr;
                }
                try
                {
                    //创建文件流  
                    fs = new FileStream(path,  //文件路径  
                                        FileMode.Open, //打开文件的方式  
                                        FileAccess.ReadWrite, //控制对文件的读写  
                                        FileShare.None);  //控制其它进程对此文件的访问  
                    //创建读取器  
                    sr = new StreamReader(fs,  //文件流对象  
                                          Encoding.Default);  //字符编码  
                    string line = "";
                    while ((line = sr.ReadLine()) != null)
                    {
                        listStr.Add(line);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("文件操作异常：" + ex.Message);
                }
                finally
                {
                    if (fs != null)
                    {
                        sr.Close(); //关闭读取器  
                        fs.Close(); //关闭文件流  
                    }
                }
            }
            return listStr;
        }
        #endregion
    }
}
