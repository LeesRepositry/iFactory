using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;
using System.Security.Cryptography;
using System.Configuration;
using System.Text.RegularExpressions;

using BiaoQianPrint.Model;
using BiaoQianPrint.sys;

using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using BiaoQianPrint.YeWu;
namespace BiaoQianPrint.Tools
{
    public enum FilePathType : byte
    {
        BOMFilePath, YLFilePath, SHFilePath,ZCM
    }
    public class CommonClass
    {

        //当前 登录帐号1，帐号2；  当前连接的加密机对象。
        public static Mdl_UserRow mCurrentUser = new Mdl_UserRow();
        public static string SHDBpath = System.Windows.Forms.Application.StartupPath + @"\SHDB";
        public static string YLDBpath = System.Windows.Forms.Application.StartupPath + @"\YLDB";
        public static string BOMDBpath = System.Windows.Forms.Application.StartupPath + @"\BOMDB";
        public static string BQDBpath = System.Windows.Forms.Application.StartupPath + @"\BQDB";//标签数据
        public static string MainDatapath = System.Windows.Forms.Application.StartupPath + @"\MainData.txt";

        //你懂的
        public bool ZCMYZ()
        {
            CZCM zcm = new CZCM();
            string strZCM = zcm.returnZCM(zcm.testGetJQM());
            if (strZCM != Ctrl_MainData.getUserInfoInstance().getFilePath(FilePathType.ZCM))
            {
                MessageBox.Show("注册码错误，不能验证通过！");
                return false;
            }
            return true;
        }

        #region 泛型方法  序列化。
        private static List<T> ReadDataFormFile<T>(string path)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            List<T> list;
            using (FileStream fileStream = new FileStream(path, FileMode.Open))
            {
                list = binaryFormatter.Deserialize(fileStream) as List<T>;
                fileStream.Close();
            }
            return list;
        }

        private static void WriteDataToFile<T>(List<T> list, string path)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (FileStream fileStream = new FileStream(path, FileMode.Create))
            {
                binaryFormatter.Serialize(fileStream, list);
                fileStream.Close();
            }
        }
        #endregion

        

        #region 获取新的编号。    必须是4位编号，第一位去掉。  第一个是List，第二个是  编号的属性名
        public static string getNewID<T>(List<T> pList,string pPropertyName)
        {
            int MaxNum = pList.Count;
            //遍历整个List，找到最大的编号。
            foreach (T item in pList)
            {
                Type type = item.GetType(); //获取类型
                System.Reflection.PropertyInfo propertyInfo = type.GetProperty(pPropertyName); //获取指定名称的属性
                string value_Old = (String)propertyInfo.GetValue(item, null); //获取属性值

                int tmp = CommonClass.GetIntFromString(value_Old.Substring(3, 1));
                if (tmp > MaxNum)
                    MaxNum = tmp;
            }
            MaxNum++;
            //  组成新的编号。
            string sReturn = "R";
            if (pList.Count < 10)
                sReturn += "00" + MaxNum.ToString();
            else if (pList.Count < 100)
                sReturn += "0" + MaxNum.ToString();
            else
                sReturn += MaxNum.ToString();
            return sReturn;
        }
        #endregion

        public static DateTime GetSysTime()
        {
            //string strSQL = "select date()";
            //return DateTime.Parse(Tools.SqlDeal.GetSqlReaderResult(strSQL));
            return DateTime.Now;
        }

        public static int GetIntFromString(string pStr)
        {
            try
            {
                return Convert.ToInt32(pStr);
            }
            catch (System.Exception ex)
            {
                return 0;
            }
        }
        public static void LogInfo(string pContent, string pRemark)
        {
            //string strSQL = "INSERT INTO [LogInfo]([UserID],[UserName],[UserID2],[UserName2],[OperationTime],[OperationContent],[Remark])VALUES" +
            //    " ('" + mCurrentUser.UserID + "','" + mCurrentUser.UserName + "','" + mCurrentUser.UserID + "','" + mCurrentUser.UserName + "','" + DateTime.Now.ToString("yyyy-MM-dd") + "','" + pContent + "','" + pRemark + "')";
            //SqlDeal.ExecuteSQLReturn(strSQL);
        }

        public static string getValFromXml(string pName)
        {
            return System.Configuration.ConfigurationSettings.AppSettings[pName];
        }
        public static bool setValToXml(string pName, string pVal)
        {
            try
            {
                Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings[pName].Value = pVal;
                //一定要记得保存，写不带参数的config.Save()也可以
                config.Save(ConfigurationSaveMode.Modified);
                //刷新，否则程序读取的还是之前的值（可能已装入内存）
                System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            }
            catch (Exception ex)
            {
                MessageBox.Show("写入XML错误，错误描述：" + ex.Message);
                return false;
            }
            return true;
        }
        /// <summary> 
        /// 如果输入的不是英文字母或者数字或者汉字，则返回false 
        /// </summary> 
        /// <returns></returns> 
        public static bool RegexName(string input)
        {
            Regex regex = new Regex(@"^[\u4E00-\u9FFFA-Za-z0-9]+$");
            return regex.IsMatch(input);
        }

        public static string GetMD5(string str)
        {
            byte[] strBytes = Encoding.UTF8.GetBytes(str);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] outputBytes = md5.ComputeHash(strBytes);
            return BitConverter.ToString(outputBytes).Replace("-", "");
        }
    }
    public class mitem
    {
        private string mitemID = "";
        private string mitemName = "";
        private int mitemAutoID = 0;
        public mitem(string pItemID, string pItemName, int pAutoID)
        {
            mitemID = pItemID;
            mitemName = pItemName;
            mitemAutoID = pAutoID;
        }
        public string ItemID
        {
            get { return mitemID; }
        }
        public string ItemName
        {
            get { return mitemName; }
        }
        public int ItemAutoID
        {
            get { return mitemAutoID; }
        }
        public override string ToString()
        {
            return mitemName.ToString();
        }
    }
    
}
