using System;
using System.Data ;
using System.Data.SqlClient;

using System.Windows.Forms;
namespace BiaoQianPrint.Tools
{
	/// <summary>
	/// SqlDeal 的摘要说明。
    /// 此次项目修改了，不是每执行一次，都打开关闭数据库一次，而是程序开始，在MainForm-load里面打开数据库，一直开着不关闭连接，close里面关闭连接。
	/// </summary>
	public static class SqlDeal222
	{

		#region   建立，关闭数据库链接
        private static string source_1 = @"Data Source=";
        public static string source_2 = "192.168.3.10";
        private static string source_3 = @"; Initial Catalog=DBName;Persist Security Info=True;User ID=sa;Password=rj_123";
        //"Server=(local);uid=sa;pwd=sdeport.2008;database=PreStatForXH";
        //(new Sdeport.PreStatForXH.Dal.Settings()).MainDatabaseConnectionString;
		private static  SqlConnection conn;

		public static void CreateSqlCon()
		{
            string mDBName=Tools.CommonClass.getValFromXml("DBName");
            string source = source_1 + source_2 + source_3.Replace("DBName", mDBName);
            //MessageBox.Show(source);
            //if (conn == null)
			conn = new SqlConnection(source);
			if (conn.State != System.Data.ConnectionState.Open)
				try
				{
					conn.Open();
				}
				catch (Exception er)
				{
					throw new ApplicationException(er.Message);
				}
		}

        public static void CloseSqlCon()
		{
			if (conn.State == System.Data.ConnectionState.Open)
			{
				conn.Close();

			}
		}	
		#endregion
		#region  私有函数  执行SQL语句
        public static bool ExecuteSQLReturn(string SQLstr)
		{
			bool flag = false;
            CreateSqlCon();
			try
			{
				SqlCommand cmd = new SqlCommand();
				cmd.Connection = conn;
				cmd.CommandText = SQLstr;
				cmd.ExecuteNonQuery();

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
			finally
			{
				flag = true;
                CloseSqlCon();
			}
			return flag;
		}
		//*****根据Sql命令返回查询结果dataset类型*****
        public static DataSet ExecuteSQLStr(string SQLstr)
		{

            CreateSqlCon(); //建立连接

			try
			{
				//建立命令
				SqlCommand cmd = new SqlCommand();
				cmd.Connection = conn;
				cmd.CommandText = SQLstr;
				//建立数据适配器 
				SqlDataAdapter da = new SqlDataAdapter();
				da.SelectCommand = cmd;
				//建立数据集
				DataSet ds = new DataSet();
				//填充数据集
				da.Fill(ds);
				return ds;
			}
			catch (Exception e)
			{
				throw new Exception(e.Message);

			}
			finally
			{
                CloseSqlCon();
			}
		}
        public static string GetSqlReaderResult(string SQLstr)
		{   //建立连接
			string mResult;
            CreateSqlCon();

			try
			{
				//建立命令
				SqlCommand cmd = new SqlCommand();
				cmd.Connection = conn;
				cmd.CommandText = SQLstr;
				//建立数据适配器 
				SqlDataReader dr = cmd.ExecuteReader();
				if (dr.Read())
				{
					mResult = dr[0].ToString();
                    dr.Close();
					return mResult;
				}
				else
				{
					mResult = "";
                    dr.Close();
					return mResult;
				}

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
			finally
			{
                CloseSqlCon();
			}
		}
		#endregion		

        #region 返回指定sql语句的datatable    DataTable GetSqlDataTable(string sqlstr)
        public static DataTable GetSqlDataTable(string sqlstr)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                CreateSqlCon();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlstr;
                da.SelectCommand = cmd;
                da.Fill(dt);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseSqlCon();
            }
            return dt;
        }
        #endregion

		#region 存储过程数据库操作  返回一个DataSet
        public static  DataSet ExecuteSPStr(string pSPName, string pSPPNameList, string pSPPValueList)
		{
			Int32 I;        
			System.Data.DataSet ds = new DataSet();
            CreateSqlCon();
			try
			{            
				SqlCommand myCmd = new SqlCommand();
				myCmd.Connection = conn;  //指定SqlConnection
				myCmd.CommandText = pSPName;//指定存储过程名称
				myCmd.CommandTimeout  =120;
				myCmd.CommandType = CommandType.StoredProcedure;//说明此SqlCommand执行的是存储过程。
				//此属性在asp中可以使用4代替
				SqlParameter sqlP = new SqlParameter();//声明一个Parameter
				//分解字符串，给属性赋值
				string[] temPNameList = pSPPNameList.Split('|');
				string[] temPValueList = pSPPValueList.Split('|');
				for (I = 0; I < temPNameList.Length ; I++)
				{
					sqlP = myCmd.Parameters.Add("@" + temPNameList[I], SqlDbType.VarChar, 20);//一般参数的指定
					sqlP.Value = temPValueList[I];
				}

				SqlDataAdapter ada = new SqlDataAdapter(myCmd);
				ada.Fill(ds);
			}
			catch (Exception e)
			{

				throw new Exception(e.Message);
			}
			finally
			{
                CloseSqlCon();
			}
			return ds;
		}
        //  返回是否执行成功
        public static bool ExecuteSPReturn(string pSPName, string pSPPNameList, string pSPPValueList)
		{
			Int32 I;
			bool bl = false;
            CreateSqlCon();
			try
			{            
				SqlCommand cmd = new SqlCommand();
				cmd.Connection = conn;
				//cmd.CommandText = SQLstr;
				//cmd.ExecuteNonQuery();
				cmd.CommandText = pSPName;//指定存储过程名称
				cmd.CommandType = CommandType.StoredProcedure;//说明此SqlCommand执行的是存储过程。
				//此属性在asp中可以使用4代替
				SqlParameter sqlP = new SqlParameter();//声明一个Parameter

				//分解字符串，给属性赋值
				string[] temPNameList = pSPPNameList.Split('|');
				string[] temPValueList = pSPPValueList.Split('|');
				for (I = 0; I < temPNameList.Length ; I++)
				{
					sqlP = cmd.Parameters.Add("@" + temPNameList[I], SqlDbType.VarChar, 20);//一般参数的指定
					sqlP.Value = temPValueList[I];
				}
				cmd.ExecuteNonQuery();
				bl = true;
			}
			catch (Exception e)
			{
				bl = false;
				throw new Exception(e.Message);
			}
			finally
			{
                CloseSqlCon();
			}
			return bl;
		}
        public static string GetSPResult(string pSPName, string pSPPNameList, string pSPPValueList, string pOutParaname)
		{
			Int32 I;
			string mResult;
            CreateSqlCon();
			try
			{
				SqlCommand cmd = new SqlCommand();
				cmd.Connection = conn;
				cmd.CommandText = pSPName;//指定存储过程名称
				cmd.CommandType = CommandType.StoredProcedure;//说明此SqlCommand执行的是存储过程。
				//此属性在asp中可以使用4代替
				SqlParameter sqlP = new SqlParameter();//声明一个Parameter

				//分解字符串，给属性赋值
				string[] temPNameList = pSPPNameList.Split('|');
				string[] temPValueList = pSPPValueList.Split('|');
				for (I = 0; I < temPNameList.Length ; I++)
				{
                    if (temPNameList[I].Length > 0)
                    {
                        sqlP = cmd.Parameters.Add("@" + temPNameList[I], SqlDbType.VarChar, 20);//一般参数的指定
                        sqlP.Value = temPValueList[I];
                    }
				}

				sqlP = cmd.Parameters.Add("@" + pOutParaname, SqlDbType.VarChar, 100);//存储过程中的输出参数
				sqlP.Direction = ParameterDirection.Output;

				cmd.ExecuteNonQuery();
				mResult = cmd.Parameters["@" + pOutParaname].Value.ToString();
			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
			finally
			{
                CloseSqlCon();
			}
			return mResult;
		}   
		#endregion
		#region 给单据编号的通用函数
		/* '======================================
			 '类模块名称：CCreateID
			 '作用：给单据编号的通用函数
			 '作者：王子同
			 '日期：2002-09-03   16：47
			 '最新日期 ： 2002-09-03 16：47
			 '方式：根据日期pDate，再根据数据库表名mInfo，单据号的列名mID查出当天的最大流水号
			 '      CreateID=mStr+pDate+最新流水号（4位）
			 '      最新流水号=当天的最大流水号+1
			 '返回值：CreateID
			 '======================================*/
        public static string CreateID(string mID, string mInfo, string mPreStr, int mNoLen)
		{
            //获取当前最大编号
            string strSQL = "select MAX(" + mID + ") from " + mInfo + " where LEFT(" + mID + "," + mPreStr.Length + ")='" + mPreStr + "'";
            string strtmp = GetSqlReaderResult(strSQL);
            //转换为数字，并+1
            strtmp = strtmp.Replace(mPreStr, "");
            int inttmp = 0;
            int.TryParse(strtmp, out inttmp);
            inttmp++;
            strtmp = inttmp.ToString();
            //循环前面加字符‘0’ 来补足位数，
            while (strtmp.Length < mNoLen)
            {
                strtmp = "0" + strtmp;
            }
            //返回合适的值
            return mPreStr+strtmp;
		}
		/* '======================================
			 '类模块名称：CCreateID
			 '作用：给单据编号的通用函数
			 '作者：王子同
			 '日期：2002-09-03   16：47
			 '最新日期 ： 2002-09-03 16：47
			 '方式：根据日期pDate，再根据数据库表名mInfo，单据号的列名mID查出当天的最大流水号
			 '      CreateID=mStr+pDate+最新流水号（4位）
			 '      最新流水号=当天的最大流水号+1
			 '返回值：CreateID
			 '======================================*/
        public static string CreateIDByDate(string mID, string mInfo, string mStr, int mLen, string mDate)
		{
			//1.先 得到固定部分，SD20090217
			//2.找到 表的最大编号  并判断是否为空，表示0001
			//3.找到SerialList表的最大编号（TableName='EpOrderDesc'），
			//4.判断 2者取哪个好
			//5.+1
			//6.补足0
			//7.返回
			int mI;// As Integer
			string mFormat;// As String
			//string mFormat1;// As String
			//string RandKey = "9999";

			int MaxNum;
			string mMaxID;
			string mTmpMaxID;
			string strSQL;
			if (mStr.Trim() == "")
			{
				return "";
			}
			mLen = 4;
			//开始了1 
			mFormat = mStr + mDate;//DateTime.Now.ToString("yyyyMMdd"); //strSQL.ToString("yyyyMMdd"); //& Format(CDate(pdate), "YYYYMMDD")

			
			strSQL = "select Max( " + mID + " ) from " + mInfo + " where " + mID + " like '" + mFormat + "'+'____' ";
			mMaxID = GetSqlReaderResult(strSQL);
			if (mMaxID == "")
			{
				mMaxID = "0000";
			}
			else
			{
				mMaxID = mMaxID.Substring(10, mLen);
			}

			strSQL = "select Max(IDSerial) from SerialList Where TableName='" + mInfo + "' And IDSerial like '" + mFormat + "'+'____' ";
			mTmpMaxID = GetSqlReaderResult(strSQL);
			if (mTmpMaxID == "")
			{
				mTmpMaxID = "0000";
			}
			else
			{
				mTmpMaxID = mTmpMaxID.Substring(10, mLen);
			}

			if (Convert.ToInt16(mTmpMaxID) > Convert.ToInt16(mMaxID))
			{
				mMaxID = mTmpMaxID;
			}

			if (mMaxID == "0000")
			{
				mMaxID = mFormat + "0001";
			}
			else
			{
				MaxNum = Convert.ToInt16(mMaxID);
				MaxNum = MaxNum + 1;
				mMaxID = MaxNum.ToString();
				for (mI = 1; mI <= mLen; mI++)
				{
					if (mMaxID.Length < 4)
					{
						mMaxID = "0" + mMaxID;
					}
				}
				mMaxID = mFormat + mMaxID;
			}
			strSQL = "Insert Into SerialList values('" + mInfo + "','" + mMaxID + "','9999')";
			ExecuteSQLReturn(strSQL);
			return mMaxID;
		}
		#endregion
	}
}
