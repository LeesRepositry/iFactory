﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using DevComponents.DotNetBar;

using BiaoQianPrint.YeWu;
using BiaoQianPrint.BaseData;
using BiaoQianPrint.sys;

namespace BiaoQianPrint
{
    public partial class MDIForm : DevComponents.DotNetBar.Office2007RibbonForm 
    {
       
        public MDIForm()
        {
            InitializeComponent();
            
        }


        private bool isLoadedForm=false;
       

        private void MDIForm_Load(object sender, EventArgs e)
        {
            superTabControl1.Tabs.Clear();
        }
        #region 创建或者显示一个多文档界面页面
        /// <summary>
        /// 创建或者显示一个多文档界面页面
        /// </summary>
        /// <param name="caption">窗体标题</param>
        /// <param name="formType">窗体类型</param>
        public void SetMdiForm(Office2007Form frm)
        {
            isLoadedForm = false;
            bool IsOpened = false;

            //遍历现有的Tab页面，如果存在，那么设置为选中即可
            foreach (SuperTabItem tabitem in superTabControl1.Tabs)
            {
                if (tabitem.Name == frm.Text.ToString())
                {
                    superTabControl1.SelectedTab = tabitem;
                    IsOpened = true;
                    break;
                }
            }

            //如果在现有Tab页面中没有找到，那么就要初始化了Tab页面了
            if (!IsOpened)
            {
                


                SuperTabItem tabItem = superTabControl1.CreateTab(frm.Text.ToString()+"|"+frm.Tag.ToString());
                tabItem.Tag = frm.Tag;
                tabItem.Name = frm.Text.ToString();
                tabItem.Text = frm.Text.ToString();
                frm.FormBorderStyle = FormBorderStyle.None;
                frm.TopLevel = false;
                frm.Visible = true;
                frm.Dock = DockStyle.Fill;
                tabItem.AttachedControl.Controls.Add(frm);
                superTabControl1.SelectedTab = tabItem;//  本语句触发：一个验证事件。就是切换到其它页上时，需要输入一次。
                frm.BackColor = Color.LightSteelBlue;


                //2016-08-11  如果 第二个没有权限，就直接不能打开。



                //增加，点击时，触发验证权限事件。
                //tabItem.Click += new EventHandler(tabItem_Click);
                isLoadedForm = true;
                return;
            }
        }
        #endregion
        private void plugin_SubEvent(int pEventType)
        {
            if (pEventType == 1)// 最小化
            {
                this.WindowState = FormWindowState.Minimized;
            }
        }
        private void buttonItem15_Click(object sender, EventArgs e)
        {
           
        }
        //BOM管理
        private void buttonItem14_Click(object sender, EventArgs e)
        {
            frm_BOMManage frm = new frm_BOMManage();
            frm.Tag = "T201";
            SetMdiForm(frm);

            //if (Tools.KMSCommon.IsHaveAuth("T201"))
            //{
               
            //}
        }
        //权限分配
        private void buttonItem16_Click(object sender, EventArgs e)
        {
            frmAuthManager frm = new frmAuthManager();
            frm.Tag = "T103";
            SetMdiForm(frm);
            //if (Tools.KMSCommon.IsHaveAuth("T103"))
            //{
                
            //}
        }
        //DC管理
        private void buttonItem17_Click(object sender, EventArgs e)
        {
            FrmDCManage frm = new FrmDCManage();
            frm.Tag = "T202";
            SetMdiForm(frm);
            //if (Tools.KMSCommon.IsHaveAuth("T202"))
            //{
            //    SetMdiForm(frm);
            //}
        }

        private void buttonItem18_Click(object sender, EventArgs e)
        {

        }

        private void MDIForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //SqlDeal.CloseSqlCon_OneTime();
        }
        //用户管理
        private void buttonItem15_Click_1(object sender, EventArgs e)
        {
            frmUserManager frm = new frmUserManager();
            frm.Tag = "T101";
            SetMdiForm(frm);
            //if (Tools.KMSCommon.IsHaveAuth("T101"))
            //{
            //    SetMdiForm(frm);
            //}
        }
        //标签管理
        private void buttonItem18_Click_1(object sender, EventArgs e)
        {
            //frm_BiaoQianManage frm = new frm_BiaoQianManage();
            //frm.Tag = "T301";
            //SetMdiForm(frm);
            //if (Tools.KMSCommon.IsHaveAuth("T301"))
            //{
            //    SetMdiForm(frm);
            //}
        }
        //角色管理
        private void buttonItem19_Click(object sender, EventArgs e)
        {
            frmRoleManager frm = new frmRoleManager();
            frm.Tag = "T102";
            SetMdiForm(frm);
            //if (Tools.KMSCommon.IsHaveAuth("T102"))
            //{
                
            //    SetMdiForm(frm);
            //}
        }
        //帐号审定
        private void buttonItem20_Click(object sender, EventArgs e)
        {
            frmUserCheckManager frm = new frmUserCheckManager();
            frm.Tag = "T104";
            SetMdiForm(frm);
            //if (Tools.KMSCommon.IsHaveAuth("T104"))
            //{
            //    SetMdiForm(frm);
            //}
        }


        private void btnLogSecond_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonItem1_Click(object sender, EventArgs e)
        {
            //frmLogin frmlog = new frmLogin(2);
            //if (frmlog.ShowDialog() == DialogResult.OK)
            //{
            //    this.Text = "密钥管理系统    当前登录账号：" + Tools.KMSCommon.mCurrentUser1.UserName + "  第二位登录账号：" + Tools.KMSCommon.mCurrentUser2.UserName;
            //}
        }
        //修改密码
        private void buttonItem3_Click(object sender, EventArgs e)
        {
            //frmLogUpdatePassword frm = new frmLogUpdatePassword(Tools.KMSCommon.mCurrentUser1.UserName);
            //frm.ShowDialog();
        }
        //收货管理
        private void buttonItem22_Click(object sender, EventArgs e)
        {
            frm_ShouHuoManage frm = new frm_ShouHuoManage();
            frm.Tag = "T001";
            SetMdiForm(frm);
            //if (Tools.KMSCommon.IsHaveAuth("T001"))
            //{
                
            //    SetMdiForm(frm);
            //}
        }

        //PN号查询
        private void buttonItem7_Click(object sender, EventArgs e)
        {
            FrmPNInfo frm = new FrmPNInfo();
            frm.Tag = "T203";
            SetMdiForm(frm);
            //if (Tools.KMSCommon.IsHaveAuth(frm.Tag.ToString()))
            //{
                
            //    SetMdiForm(frm);
            //}
        }


        private void superTabControl1_SelectedTabChanged(object sender, SuperTabStripSelectedTabChangedEventArgs e)
        {
            //遍历现有的Tab页面，如果存在，那么设置为选中即可

            //  如果还没加载完tableitem 就等待。
            //Thread.Sleep(500); 

            //try
            //{
            //    foreach (SuperTabItem tabitem in superTabControl1.Tabs)
            //    {
            //        if (tabitem.IsSelected == true)
            //        {
            //            //string[] splitTmp=tabitem.Text.ToString().Split('|');
            //            //string mAuthID = splitTmp[1];
            //            if (Tools.KMSCommon.IsHaveAuth(tabitem.Tag.ToString()) != true)//
            //            {
            //                tabitem.Close();
            //            }
            //        }
            //    }
            //}
            //catch (System.Exception ex)
            //{
            	
            //}
        }

        private void buttonItem13_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnInit_Click(object sender, EventArgs e)
        {
            //frmInitSys frm = new frmInitSys();
            //frm.ShowDialog();
            //if (frm.DialogResult == DialogResult.OK)
            //{
            //    frm.Close();
            //}
        }

        private void buttonItem4_Click(object sender, EventArgs e)
        {

        }

        private void buttonItem6_Click(object sender, EventArgs e)
        {

        }
        private void buttonItem4_Click_1(object sender, EventArgs e)
        {
            frm_BiaoQianCreat frm = new frm_BiaoQianCreat();
            frm.Tag = "T301";
            SetMdiForm(frm);
        }

        private void ribbonBar9_ItemClick(object sender, EventArgs e)
        {
            frm_BiaoQianPrint frm = new frm_BiaoQianPrint();
            frm.Tag = "T301";
            SetMdiForm(frm);
        }

        private void buttonItem5_Click(object sender, EventArgs e)
        {
            frm_YLManage frm = new frm_YLManage();
            frm.Tag = "T301";
            SetMdiForm(frm);
        }

        private void buttonItem6_Click_1(object sender, EventArgs e)
        {

        }
    }
}
